%% Queue Class for (x,y) Coordinates
% Author: Connor Morales
% 
% Properties:
%   Data  -> Cell array of data stored
%   IndexFront -> Index of front of queue
%	IndexBack  -> Index of back of queue
% 
% Methods:
%   Reset   -> resets queue and empties
%   isEmpty -> boolean of whether queue is empty or not
%   Count   -> number of elements in queue
%   Enqueue -> adds data to back of queue
%   Dequeue -> reads data from front of queue (empty matrix if none)
%   HasData -> checks if data is in queue
%   PeekAll -> returns list of all data in queue
% 

classdef Queue < handle
	
	properties(GetAccess = public, SetAccess = private)
		% Cell array of data
		Data
		% Index of current cell
		IndexFront
		IndexBack
	end
	
	methods(Access = public)
		
		function obj = Queue()
			obj.Data = {};
			obj.IndexFront = uint64(1);
			obj.IndexBack  = uint64(1);
		end
		
		% Reset queue to empty
		function Reset(obj)
			obj.Data = {};
			obj.IndexFront = uint64(1);
			obj.IndexBack = uint64(1);
		end
	
		% Check if queue is empty
		function b = isEmpty(obj)
			b = (obj.IndexFront == obj.IndexBack);
		end
		
		% Determine number of elements in queue
		function n = Count(obj)
			n = (obj.IndexBack - obj.IndexFront);
		end
	
		% Add data to back of queue
		function Enqueue(obj, toAdd)
			% Add new data
			obj.Data{obj.IndexBack} = toAdd;
			% Shift index for back of queue
			obj.IndexBack = obj.IndexBack + 1;
		end
		
		% Read data from front of queue
		function toRet = Dequeue(obj)
			if obj.IndexFront ~= obj.IndexBack
				% Save output data
				toRet = obj.Data{obj.IndexFront};
				% Clear queue data
				obj.Data{obj.IndexFront} = [];
				% Shift index for front of queue
				obj.IndexFront = obj.IndexFront + 1;
				% Reset queue if empty
				if obj.IndexFront == obj.IndexBack
					obj.Reset();
				end
			else
				toRet = [];
			end
		end
		
		% Check if data exists in queue
		function b = HasData(obj, data)
			b = false;
			for i = obj.IndexFront : obj.IndexBack-1
				if isequal(obj.Data{i}, data)
					b = true;
					break;
				end
			end
		end
		
		% Peek at all the data without removing it
		% Will be size (Nx2) with each row (x,y)
		function toRet = PeekAll(obj)
			% Initialize output to empty
			toRet = [];
			% Add each element to output
			for i = obj.IndexFront : obj.IndexBack-1
				toRet = [toRet; [obj.Data{i}(1),obj.Data{i}(2)]];
			end
		end
		
	end
	
end


