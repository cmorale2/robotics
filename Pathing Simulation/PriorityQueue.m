%% Proirity Queue Class for (x,y) Coordinates
% Author: Connor Morales
% 
% This is a basic priority queue for coordiantes, to be using with the pathing
% simulator.  The smallest priorities will be returned first.  If multiple data
% entries have the same priority, they will be returned in the order they were
% insterted.
% 
% Properties:
%   Data       -> Cell array of data stored (empty matrix if removed)
%   Priorities -> Vector of data priority (NaN if data removed)
% 
% Methods:
%   Reset   -> resets queue and empties
%   isEmpty -> boolean of whether queue is empty or not
%   Count   -> number of elements in queue
%   Enqueue -> adds data to back of queue
%   Dequeue -> reads data from front of queue (empty matrix if none)
%   HasData -> checks if data is in queue
%   PeekAll -> returns list of all data in queue
% 

classdef PriorityQueue < handle
	
	properties(GetAccess = public, SetAccess = private)
		% Cell array of data
		Data
		Priorities
	end
	
	methods(Access = public)
		
		function obj = PriorityQueue()
			obj.Data = {};
		end
		
		
		
		% Reset queue to empty
		function Reset(obj)
			obj.Data = {};
		end
	
		
		
		% Check if queue is empty
		function b = isEmpty(obj)
			b = isempty(obj.Priorities) || all(isnan(obj.Priorities));
		end
		
		
		
		% Determine number of elements in queue
		function n = Count(obj)
			n = sum(~isnan(obj.Priorities));
		end
	
		
		
		% Add data to queue
		function Enqueue(obj, toAdd, p)
			
			% Add data
			obj.Data = [obj.Data, toAdd];
			obj.Priorities = [obj.Priorities, p];
			
		end
		

		% Get data from queue
		function varargout = Dequeue(obj)
			
			% Initialize output
			dataOut = [];
			pOut = [];
			
			% Get list of smallest priorities.  Since this is a queue, we want
			% the first index (first inserted)
			[~,ind] = min(obj.Priorities);
			
			% Assign data
			dataOut = obj.Data{ind};
			pOut = obj.Priorities(ind);
			
			% Clear queue information to be safe and remove from priority list
			obj.Data{ind} = [];
			obj.Priorities(ind) = NaN;
			
			% Check if queue is empty
			if all(isnan(obj.Priorities))
				obj.Data = {};
				obj.Priorities = [];
			end
			
			% Set output
			if nargout > 0
				varargout{1} = dataOut;
				if nargout > 1
					varargout{2} = pOut;
				end
			end
			
		end
		
		
		% Check if data exists in queue
		function b = HasData(obj, data)
			b = false;
			for i = 1 : length(obj.Data)
				if isequal(obj.Data{i}, data)
					b = true;
					break;
				end
			end
		end
		
		
		
		% Peek at all the data without removing it
		% Will be size (Nx2) with each row (x,y)
		function toRet = PeekAll(obj)
			% Initialize output to empty
			toRet = [];
			% Add each element to output
			for i = 1 : length(obj.Data)
				if ~isnan(obj.Priorities(i))
					toRet = [toRet; [obj.Data{i}(1),obj.Data{i}(2)]];
				end
			end
		end
		
	end
	
end


