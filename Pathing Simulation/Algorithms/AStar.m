%% Algorithm - A*
% Author: Connor Morales
% 
% Description:
%   This function implements A* pathing algorithm.  The heuristic distance
%   algorithm is located at the bottom of the file, and defaults to manhattan
%   distance.
% 

function [data, map] = AStar(data, map, barriers, start, goal)

	% Initialization process
	if ~data.init
		
		% Mark structure as initialized
		data.init = true;
		
		% Size of map
		[data.h, data.w] = size(map);
		
		% Queue as needed by algorithm (generic or priority).  All cells will
		% show up on map, so should be stored as (x,y)
		data.frontier = PriorityQueue();
		
		% Matrix denoting if we have visited the cell or not.  Will show up on
		% map, so indexing is same as 'map'
		data.visited = false(data.h, data.w);
		
		% Current cell being considered by algorithm.  Will show up on map, so
		% should be stored as (x;y)
		data.current = start;
		
		% Path as decided by algorithm.  Should be size (Nx2) with each row
		% (x,y) denoting a coordinate.  The rows should be in order from either
		% start to goal or goal to start.
		data.path = [];
		
		% Location we came from
		data.from = cell(data.h, data.w);
		
		% Start location is first cell
		data.frontier.Enqueue(start, 0);
		data.visited(start(2), start(1)) = true;
		data.from{start(2), start(1)} = '\0';
		map(start(2), start(2)) = 0;
		
		% Initialize previous cell to none
		data.current = start;
		
	end
	
	% Grab next cell from queue
	data.current = data.frontier.Dequeue();
	curcost = map(data.current(2), data.current(1));
	newcost = curcost + 1;
	
	% Mark as visited
	data.visited(data.current(2), data.current(1)) = true;
	
	
	% End condition
	if isequal(data.current, goal)
		
		% Populate path, start at goal and work backwards
		cur = goal;
		data.path = [cur(1), cur(2)];
		
		% Continue until we reach the start
		while ~isequal(cur, start)
			
			% Get next cell
			x = cur(1);
			y = cur(2);
			if strcmp(data.from{y,x}, 't')
				cur = [x; y+1];
			elseif strcmp(data.from{y,x}, 'b')
				cur = [x; y-1];
			elseif strcmp(data.from{y,x}, 'r')
				cur = [x+1; y];
			elseif strcmp(data.from{y,x}, 'l')
				cur = [x-1; y];
			else
				error('Unknown from character');
			end
			
			% Add cell to path
			data.path = [data.path; cur(1),cur(2)];
		end
		
		% Mark as complete
		data.done = true;
		
		return;
	end
	
	
	
	
	
	% Add all neighbors to queue
	
	% Right
	x = data.current(1)+1;
	y = data.current(2);
	coord = [x;y];
	if x<=data.w && ~barriers(y,x) && (~data.visited(y,x) || newcost<map(y,x)) && ~data.frontier.HasData(coord)
		dist = distance(coord, goal);
		data.frontier.Enqueue(coord, newcost+dist);
		data.from{y,x} = 'l';
		map(y,x) = newcost;
	end
	
	% Top
	x = data.current(1);
	y = data.current(2)+1;
	coord = [x;y];
	if y<=data.h && ~barriers(y,x) && (~data.visited(y,x) || newcost<map(y,x)) && ~data.frontier.HasData(coord)
		dist = distance(coord, goal);
		data.frontier.Enqueue(coord, newcost+dist);
		data.from{y,x} = 'b';
		map(y,x) = newcost;
	end
	
	% Left
	x = data.current(1)-1;
	y = data.current(2);
	coord = [x;y];
	if x>=1 && ~barriers(y,x) && (~data.visited(y,x) || newcost<map(y,x)) && ~data.frontier.HasData(coord)
		dist = distance(coord, goal);
		data.frontier.Enqueue(coord, newcost+dist);
		data.from{y,x} = 'r';
		map(y,x) = newcost;
	end
	
	% Bottom
	x = data.current(1);
	y = data.current(2)-1;
	coord = [x;y];
	if y>=1 && ~barriers(y,x) && (~data.visited(y,x) || newcost<map(y,x)) && ~data.frontier.HasData(coord)
		dist = distance(coord, goal);
		data.frontier.Enqueue(coord, newcost+dist);
		data.from{y,x} = 't';
		map(y,x) = newcost;
	end
	
end



% Heuristic distance
function d = distance(a,b)
	% Manhattan distance
	d = abs(a(1)-b(1)) + abs(a(2)-b(2));
	
	% Euclidian distance
% 	d = sqrt( (a(1)-b(1))^2 + (a(2)-b(2))^2 );
end


