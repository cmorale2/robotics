%% Algorithm - Demo
% Author: Connor Morales
% 
% Description:
%   This function is a demo file for the pathing simulator.  Every time the
%   function is called, it should perform a single step of the pathing
%   algorithm.
% 
%   Any data that needs to be retained between algorithm steps should be stored
%   in the 'data' input.  This is a structure that will be passed back to the
%   algorithm at the next step.  It has some fields that are used to draw the
%   map and determine when the algorithm has completed.  See comments in the
%   function below for more details.
% 
%   The 'map' variable is drawn to the simulator grid.  It should be used to
%   hold travel costs used by the algorithm, if appropriate.  If not needed by
%   the algorithm, it can be ignored.
% 
%   The 'barriers' input is a logical matrix containing information about
%   barriers in the map.  Indices in 'barriers' correspond to indices in 'map'.
%   The algorithm should not allow a path that goes through a barrier.
% 
%   The 'start' and 'goal' variables contain the starting location and ending
%   location for the desired path.  They are column vectors (size 2x1).
% 
% 
% Inputs:
%   (1) - Algorithm data structure
%   (2) - Map of travel costs
%   (3) - Logical matrix of map barriers
%   (4) - Start location (x,y)
%   (5) - Goal location (x,y)
% 
% Outputs:
%   (1) - Updated algorithm data structure
%   (2) - Updated map of travel costs
% 

function [data, map] = Demo(data, map, barriers, start, goal)


	% If the data struct is not initialized, we are at the first step of the
	% algorithm, so do default init stuff
	if ~data.init
		fprintf('Algorithm initialization...\n');
		
		% Mark structure as initialized
		data.init = true;
		
		% Queue as needed by algorithm (generic or priority).  All cells will
		% show up on map, so should be stored as (x,y)
		data.frontier = Queue();
		
		% Matrix denoting if we have visited the cell or not.  Will show up on
		% map, so indexing is same as 'map'
		data.visited = false(size(map));
		
		% Current cell being considered by algorithm.  Will show up on map, so
		% should be stored as (x,y)
		data.current = start;
		
		% Path as decided by algorithm.  Should be size (Nx2) with each row
		% (x,y) denoting a coordinate.  The rows should be in order from either
		% start to goal or goal to start.
		data.path = [];
		
		% Demo varaible
		data.counter = 1;
	end
	
	
	% Check break condition
	if data.counter >= 5
		
		% Demo path only including start/goal
		data.path = [start'; goal'];
		
		% Mark algorithm as completed
		data.done = true;
		
	end
	
	
	
	% Perform algorithm step
	fprintf('Algorithm step...\n');
	data.counter = data.counter + 1;
	map(randi(25), randi(25)) = randi(64);
	

end
