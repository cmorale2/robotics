%% Algorithm - Breadth-First Search
% Author: Connor Morales
% 
% Description:
%   This function performs a breadth-first search pathing algorithm.
% 

function [data, map] = BreadthFirst(data, map, barriers, start, goal)

	% Initialization process
	if ~data.init
		
		% Mark structure as initialized
		data.init = true;
		
		% Size of map
		[data.h, data.w] = size(map);
		
		% Queue as needed by algorithm (generic or priority).  All cells will
		% show up on map, so should be stored as (x,y)
		data.frontier = Queue();
		
		% Matrix denoting if we have visited the cell or not.  Will show up on
		% map, so indexing is same as 'map'
		data.visited = false(data.h, data.w);
		
		% Current cell being considered by algorithm.  Will show up on map, so
		% should be stored as (x;y)
		data.current = start;
		
		% Path as decided by algorithm.  Should be size (Nx2) with each row
		% (x,y) denoting a coordinate.  The rows should be in order from either
		% start to goal or goal to start.
		data.path = [];
		
		% Location we came from
		data.from = cell(data.h, data.w);
		
		% Start location is first cell
		data.frontier.Enqueue(start);
		data.visited(start(2), start(1)) = true;
		data.from{start(2), start(1)} = '\0';
		
		% Initialize previous cell to none
		data.current = start;
		
	end
	
	
	
	% Grab next cell from queue
	data.current = data.frontier.Dequeue();
	
	% Mark as visited
	data.visited(data.current(2), data.current(1)) = true;
	
	
	% End condition
	if isequal(data.current, goal)
		
		% Populate path, start at goal and work backwards
		cur = goal;
		data.path = [cur(1), cur(2)];
		
		% Continue until we reach the start
		while ~isequal(cur, start)
			
			% Get next cell
			x = cur(1);
			y = cur(2);
			if strcmp(data.from{y,x}, 't')
				cur = [x; y+1];
			elseif strcmp(data.from{y,x}, 'b')
				cur = [x; y-1];
			elseif strcmp(data.from{y,x}, 'r')
				cur = [x+1; y];
			elseif strcmp(data.from{y,x}, 'l')
				cur = [x-1; y];
			else
				error('Unknown from character');
			end
			
			% Add cell to path
			data.path = [data.path; cur(1),cur(2)];
		end
		
		% Mark as complete
		data.done = true;
		
		return;
	end
	
	
	
	
	
	% Add all neighbors to queue
	% Top
	x = data.current(1);
	y = data.current(2)+1;
	coord = [x;y];
	if y<=data.h && ~data.visited(y,x) && ~data.frontier.HasData(coord) && ~barriers(y,x)
		data.frontier.Enqueue(coord);
		data.from{y,x} = 'b';
	end
	
	% Bottom
	x = data.current(1);
	y = data.current(2)-1;
	coord = [x;y];
	if y>=1 && ~data.visited(y,x) && ~data.frontier.HasData(coord) && ~barriers(y,x)
		data.frontier.Enqueue(coord);
		data.from{y,x} = 't';
	end
	
	% Right
	x = data.current(1)+1;
	y = data.current(2);
	coord = [x;y];
	if x<=data.w && ~data.visited(y,x) && ~data.frontier.HasData(coord) && ~barriers(y,x)
		data.frontier.Enqueue(coord);
		data.from{y,x} = 'l';
	end
	
	% Left
	x = data.current(1)-1;
	y = data.current(2);
	coord = [x;y];
	if x>=1 && ~data.visited(y,x) && ~data.frontier.HasData(coord) && ~barriers(y,x)
		data.frontier.Enqueue(coord);
		data.from{y,x} = 'r';
	end
	
end
