%% MapGrid - Update Map
% Author: Connor Morales
% 
% Description:
%   This function updates the map with the one provided
% 
% Inputs:
%   (1) - MapGrid object
%   (2) - Updated map
% 
% Outputs:
%   (1) - MapGrid object
% 

function [obj] = Update(obj, map)

	% Sanity check - size
	if ~isequal(size(map), size(obj.map))
		error('Cannot change map size with update');
	end
	
	% Set updated map back to object
	obj.map = map;
	
end
