%% MapGrid - Draw Map to Axes
% Author: Connor Morales
% 
% Description:
%   This function draws the map to the axis object provided.  It is assumed that
%   the axis size is such that each cell will be 20px in size.
% 
% Inputs:
%   (1) - MapGrid object
%   (2) - Axis object
%   (3) - Algorithm data structure
% 
% Outputs:
%   (1) - MapGrid object
% 

function [obj] = DrawMap(obj, f, algdata)

	% Clear axes to start
	cla(f);
	
	
	% Draw map, need to flip because of the way image works
	image(f, flipud(obj.map+1));
	colormap(f, obj.colormap);
	hold(f, 'on');
	
	
	% Mark visited cells on map
	if isfield(algdata, 'visited') && isequal(size(algdata.visited), size(obj.map))
		[y,x] = find(algdata.visited);
		% Need to flip y direction
		plot(f, x,obj.msize-y+1, 'LineStyle','none', 'Marker','.', 'MarkerSize',20, 'MarkerEdgeColor',obj.cvisited);
	end
	
	
	% Mark queued cells on map
	if isfield(algdata, 'frontier') && ~algdata.frontier.isEmpty()
		coords = algdata.frontier.PeekAll();
		x = coords(:,1);
		y = coords(:,2);
		% Need to flip y direction
		plot(f, x,obj.msize-y+1, 'LineStyle','none', 'Marker','.', 'MarkerSize',20, 'MarkerEdgeColor',obj.cqueue);
	end
	
	
	% Mark current cell on map
	if isfield(algdata, 'current') && length(algdata.current) == 2
		x = algdata.current(1);
		y = algdata.current(2);
		% Need to flip y direction
		plot(f, x,obj.msize-y+1, 'LineStyle','none', 'Marker','s', 'MarkerSize',20, 'MarkerEdgeColor','k', 'MarkerFaceColor',obj.ccurrent);
	end
	
	
	% Place barrier markers
	[y,x] = find(obj.barriers);
	if ~isempty(y)
		% Need to flip y direction
		plot(f, x,obj.msize-y+1, 'LineStyle','none', 'Marker','s', 'MarkerSize',20, 'MarkerEdgeColor','k', 'MarkerFaceColor','k');
	end
	
	
	% Place start/end markers
	if ~isempty(obj.start)
		% Need to flip y direction
		plot(f, obj.start(1), obj.msize-obj.start(2)+1, 'Marker','s', 'MarkerSize',15, 'MarkerEdgeColor','k', 'MarkerFaceColor',obj.cstart);
	end
	if ~isempty(obj.goal)
		% Need to flip y direction
		plot(f, obj.goal(1), obj.msize-obj.goal(2)+1,   'Marker','s', 'MarkerSize',15, 'MarkerEdgeColor','k', 'MarkerFaceColor',obj.cgoal);
	end
	
	
	% Place path markers
	if ~isempty(obj.path)
		% Need to flip y direction
		plot(f, obj.path(:,1), obj.msize-obj.path(:,2)+1, '.-', 'LineWidth',5, 'Color','k', 'MarkerSize',20);
		plot(f, obj.path(:,1), obj.msize-obj.path(:,2)+1, '.-', 'LineWidth',1, 'Color','w', 'MarkerSize',10);
	end
	
	
	% Draw custom grid lines
	for i = 0 : obj.msize
		% X
		plot(f, [i+0.5, i+0.5], [0.5, obj.msize+0.5], '-', 'Color', [0.5,0.5,0.5,0.5]);
		% Y
		plot(f, [0.5, obj.msize+0.5], [i+0.5, i+0.5], '-', 'Color', [0.5,0.5,0.5,0.5]);
	end
	
	
	% Clear axis markers
	set(f, 'Visible', 'off');
	
	
	% Wait for draw update
	refreshdata(f);
	
end
