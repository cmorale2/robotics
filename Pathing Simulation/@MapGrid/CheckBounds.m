%% MapGrid - Boundary Checking
% Author: Connor Morales
% 
% Description:
%   This function checks if a coordinate is inside the map bounds
% 
% Inputs:
%   (1) - MapGrid object
%   (2) - Location coordinate (x,y)
% 
% Outputs:
%   (1) - True/False flag
% 

function [TF] = CheckBounds(obj, coord)

	TF = coord(1) >= 1 && coord(1) <= obj.msize && coord(2) >= 1 && coord(2) <= obj.msize;
	
end
