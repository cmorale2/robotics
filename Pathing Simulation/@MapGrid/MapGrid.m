%% Mapgrid Class
% Author: Connor Morales



classdef MapGrid < handle
	
	% Constants
	properties (Constant = true, Hidden = true)
		msize = 25;
		colormap = [1, 1, 1; parula];
		cstart   = 'g';
		cgoal    = 'r';
		ccurrent = 'y';
		cvisited = [0,0.75,0];
		cqueue   = [0.75,0,0];
	end
	
	% Properties
	properties (SetAccess = private)
		map
		path
		start
		goal
		barriers
	end
	
	% Static Functions
	methods (Access = private, Static = true)
		TF = CheckBounds(obj, coord);
	end
	
	% Private Functions
	methods (Access = private)
		obj = BarrierInterp(obj, start, goal);
		obj = BarrierOrtho(obj, start, goal);
		obj = BarrierSuper(obj, start, goal);
	end
	
	% Public Functions
	methods (Access = public)
		obj = DrawMap(obj, ax, algdata);
		obj = LoadMap(obj,filename);
		obj = SetStart(obj, coord);
		obj = ShiftStart(obj, dir);
		obj = SetGoal(obj, coord);
		obj = ShiftGoal(obj, dir);
		obj = SetPath(obj, path);
		function obj = Reset(obj)
			obj.map = zeros(obj.msize);
			obj.path = [];
		end
	end
	
	% Constructor
	methods (Access = public)
		function obj = MapGrid(varargin)
			% Check how many inputs
			if nargin == 0
				obj.map = zeros(obj.msize);
				
			elseif nargin == 1
				if ~ischar(varargin{1})
					error('Filename must be character array');
				end
				
				obj.LoadMap(varargin{1});
				
			else
				error('Too many input arguments (%i)', nargin);
				
			end
			
			obj.barriers = false(obj.msize);

		end
	end
	
end

