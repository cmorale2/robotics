%% MapGrid - Set Path
% Author: Connor Morales
% 
% Description:
%   This function sets the final path on the map.
% 
% Inputs:
%   (1) - MapGrid object
%   (2) - Path list (nx2)
% 
% Outputs:
%   (1) - MapGrid object
% 

function [obj] = SetPath(obj, path)

	% Sanity check - sizing
	[~,c] = size(path);
	if c ~= 2
		error('The path must be of size nx2, was nx%i', c);
	end

	% Sanity checks - contains start and goal
	if ~ismember(obj.start', path)
		error('The start location must be in the path list');
	end
	if ~ismember(obj.goal', path)
		error('The goal location must be in the path list');
	end

	% Set updated map back to object
	obj.path = path;
	
end
