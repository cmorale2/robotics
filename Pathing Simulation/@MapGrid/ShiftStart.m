%% MapGrid - Shift Start Location
% Author: Connor Morales
% 
% Description:
%   This function shifts the start location in the direction specified.  If the
%   shift would result in the coordinate being out of map bounds or in a
%   boundary, a warning is emitted, and the shift does not happen.
% 
% Inputs:
%   (1) - MapGrid object
%   (2) - Direction ('u','d','l','r')
% 
% Outputs:
%   (1) - MapGrid object
% 

function [obj] = ShiftStart(obj, dir)

	% Grab start location and check if set
	coord = obj.start;
	if isempty(coord) || length(coord) ~= 2
		error('Start coordinate not yet set.');
	end

	% Shift by requested direction
	if strcmpi(dir, 'u')
		coord(2) = coord(2) + 1;
	elseif strcmpi(dir, 'd')
		coord(2) = coord(2) - 1;
	elseif strcmpi(dir, 'l')
		coord(1) = coord(1) - 1;
	elseif strcmpi(dir, 'r')
		coord(1) = coord(1) + 1;
	else
		error('Unknown direction: %s', dir);
	end
	
	% Check bounds on new coord and assign to object if valid
	if ~MapGrid.CheckBounds(obj,coord)
		warning('Shifted start location (%i,%i) out of bounds.  Not shifting.', coord(1), coord(2));
	elseif obj.barriers(coord(2), coord(1))
		warning('Shifted start location (%i,%i) into barrier.  Not shifting.', coord(1), coord(2));
	else
		obj.start = coord;
	end
	
end
