%% MapGrid - Orthogonal Walk
% Author: Connor Morales
% 
% Description:
%   This function draws a barrier on the input map using an orthogonal walk.
%   The start and end location specified must be within the map bounds.  The
%   barrier is created by stepping to the next N4 connected cell that the line
%   from the start and end passes through.  In the case of a line directly
%   through the corner of a cell, a horizontal step will be performed.
% 
% Inputs:
%   (1) - MapGrid object
%   (2) - Start location (x,y)
%   (3) - End Location (x,y)
% 
% Outputs:
%   (1) - MapGrid object
% 

function [obj] = BarrierOrtho(obj, start, goal)

	% Ensure coordinates are in map bounds
	if ~MapGrid.CheckBounds(obj,start)
		error('Bad Start (%i,%i), must be in [1, %i]', start(1), start(2), obj.msize);
	end
	if ~MapGrid.CheckBounds(obj,goal)
		error('Bad Goal (%i,%i), must be in [1, %i]', goal(1), goal(2), obj.msize);
	end

	% Loop until we have reached the end location X and Y
	dx = goal(1) - start(1);
	dy = goal(2) - start(2);
	nx = abs(dx);
	ny = abs(dy);
	sx = sign(dx);
	sy = sign(dy);
	ix=0;
	iy=0;
	p = start;
	while ix<nx || iy<ny
		
		% Place barrier at current point
		obj.barriers(p(2), p(1)) = true;
		
		% Determine whether we need to step in X or step in Y
		% In case of tie, favor horizontal step
		l = (0.5+ix)/nx <= (0.5+iy)/ny;
		
		% Perform step
		if l
			% Horizontal
			p(1) = p(1) + sx;
			ix = ix+1;
		else
			% Vertical
			p(2) = p(2) + sy;
			iy = iy+1;
		end
		
	end
	
	% Place final barrier
	obj.barriers(goal(2), goal(1)) = true;

end
