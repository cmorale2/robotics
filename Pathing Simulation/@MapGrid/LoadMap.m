%% MapGrid - Load File
% Author: Connor Morales
% 
% Description:
%   This function loads a map (barriers) from a file.  See the demo map file for
%   details on formatting.
% 
% Inputs:
%   (1) - MapGrid object
%   (2) - Filename to load
% 
% Outputs:
%   (1) - MapGrid object
% 

function [obj] = LoadMap(obj, filename)

	% Initialize map
	obj.start = [];
	obj.goal = [];
	obj.map = zeros(obj.msize);
	obj.barriers = zeros(obj.msize);

	% Open file
	f = fopen(filename, 'r');

	% Read all lines
	while true
		
		% Get next line
		fline = fgets(f);
		
		% Break condition
		if ~ischar(fline)
			break;
		end
		
		% Trim line of lead/end whitespace
		line = strtrim(fline);
		
		% Convert everything to lowercase for easy comparison
		line = lower(line);
		
		% Split line into tokens, breaking when we reach the end or a comment
		linetok = cell(0);
		while ~isempty(line) && ~strcmp(line(1), '%')
			
			[word, line] = strtok(line);
			line = strtrim(line);
			linetok = [linetok; word];
			
		end
		
		% Check if no command
		if isempty(linetok)
			continue;
		end
		
		% Handle line based on command token
		if strcmp(linetok{1}, 'barrier')
			
			% We need 5 tokens + command
			if length(linetok) ~= 6
				error('Bad line: %s', fline);
			end
			
			% Start location
			s = [str2double(linetok{2}), str2double(linetok{3})];
			% End location
			e = [str2double(linetok{4}), str2double(linetok{5})];
			
			% Interpolation based on last
			if strcmp(linetok{6}, 'interp')
				obj.BarrierInterp(s,e);
			elseif strcmp(linetok{6}, 'ortho')
				obj.BarrierOrtho(s,e);
			elseif strcmp(linetok{6}, 'super')
				obj.BarrierSuper(s,e);
			else
				error('Unknown line type: %s', linetok{6});
			end
			
			
			
		elseif strcmp(linetok{1}, 'start')
			
			% We need 2 tokens + command
			if length(linetok) ~= 3
				error('Bad line: %s', fline);
			end
			
			% Set location
			obj.SetStart([str2double(linetok{2}); str2double(linetok{3})]);
			
			
		elseif strcmp(linetok{1}, 'goal')
			
			% We need 2 tokens + command
			if length(linetok) ~= 3
				error('Bad line: %s', fline);
			end
			
			% Set location
			obj.SetGoal([str2double(linetok{2}); str2double(linetok{3})]);
			
			
		else
			error('Unknown command: %s', linetok{1});
		end
		
		
	end

	% Cleanup
	fclose(f);
	
	% Assign start and goal randomly if they weren't set by map
	if isempty(obj.start)
		obj.SetStart();
		warning('Start location not set in map, assigning randomly (%i,%i)', obj.start(1), obj.start(2));
	end
	if isempty(obj.goal)
		obj.SetGoal();
		warning('Goal location not set in map, assigning randomly (%i,%i)', obj.goal(1), obj.goal(2));
	end
	
	% Validate start and goal locations
	if isequal(obj.start, obj.goal)
		error('Start and goal locations cannot be the same');
	end
	if obj.barriers(obj.start(2), obj.start(1))
		error('Start location cannot be on a barrier (%i,%i)', obj.start(1), obj.start(2));
	end
	if obj.barriers(obj.goal(2), obj.goal(1))
		error('Goal location cannot be on a barrier (%i,%i)', obj.goal(1), obj.goal(2));
	end
	
end
