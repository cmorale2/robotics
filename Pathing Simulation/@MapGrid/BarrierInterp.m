%% MapGrid - Linear Interpolation
% Author: Connor Morales
% 
% Description:
%   This function draws a barrier on the input map using direct interpolation.
%   The start and end location specified must be within the map bounds.  The
%   barrier will contain the minimum number of cells requried to make a full
%   N8-connected line between the start and end locations.
% 
% Inputs:
%   (1) - MapGrid object
%   (2) - Start location (x,y)
%   (3) - End Location (x,y)
% 
% Outputs:
%   (1) - MapGrid object
% 

function [obj] = BarrierInterp(obj, start, goal)

	% Ensure coordinates are in map bounds
	if ~MapGrid.CheckBounds(obj,start)
		error('Bad Start (%i,%i), must be in [1, %i]', start(1), start(2), obj.msize);
	end
	if ~MapGrid.CheckBounds(obj,goal)
		error('Bad Goal (%i,%i), must be in [1, %i]', goal(1), goal(2), obj.msize);
	end
	

	% Find maximum side distance
	N = max(abs([goal(1)-start(1), goal(2)-start(2)]));
	
	% Trivial case
	if N == 0
		obj.barriers(start(2), start(1)) = true;
		return;
	end
	
	% We want N+1 cells in our interpolated line
	for i = 0 : N
		
		% Interpolate to find i^th point on line
		p = linear_interp(start, goal, i/N);
		
		% Set point on map to barrier
		obj.barriers(p(2), p(1)) = true;
		
	end

end



%% Helper Function - Interpolation Point

function [point] = linear_interp(start, goal, t)

	% Initialize
	point = [0;0];
	% X coordinate
	point(1) = start(1) + t*(goal(1)-start(1));
	% Y coordinate
	point(2) = start(2) + t*(goal(2)-start(2));
	% Force integer
	point = floor(point);

end
