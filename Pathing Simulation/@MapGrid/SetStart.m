%% MapGrid - Set Start Location
% Author: Connor Morales
% 
% Description:
%   This function sets the start location on the map.  If no coordinate is
%   provided, a random one will be selected that is not the same as the goal
%   location nor in a barrier.
% 
% Inputs:
%   (1) - MapGrid object
%   (2) - Location coordinate (x,y)
% 
% Outputs:
%   (1) - MapGrid object
% 

function [obj] = SetStart(obj, varargin)

	% Handle coordinate input
	if nargin == 1
		% No coordinate provided, select random one within bounds that is not
		% the same as the goal location or a barrier
		while true
			coord = randi(obj.msize, 2,1);
			if ~isequal(coord, obj.goal) && ~obj.barriers(coord(2), coord(1))
				break;
			end
		end
		
		
	elseif nargin == 2
		% Coordinate provided.  Ensure it is not the same as the goal location
		% or a barrier
		coord = [varargin{1}(1); varargin{1}(2)];
		if obj.barriers(coord(2), coord(1))
			error('Cannot place start on barrier (%i,%i)', coord(1), coord(2));
		end
		if isequal(coord, obj.goal)
			warning('Goal location specified is same as start (%i,%i)', coord(1), coord(2));
		end
		
	else
		error('Too many arguments provided');
	end
	
	% Ensure we are within bounds
	if ~MapGrid.CheckBounds(obj, coord)
		error('Coordinate (%i,%i) outside map bounds [1,%i]', coord(1), coord(2), obj.msize);
	end
	
	% Set start
	obj.start = coord;
	
end
