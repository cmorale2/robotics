%% EKF SLAM - Movement Update
% Author: Connor Morales
% 
% Description:
%   This function performs the movment update for EKF SLAM.  It is capable of 
%   using both ICR and FKM movement models and will detect which one is needed
%   based on input velocities.
% 
% Inputs:
%  - Current mean value vector (size 3N+3 x 1)
%  - Current covariance matrix (size 3N+3 x 3N+3)
%  - Control action (linear vel, rotational vel, time step)
%  - Movement sensor variance matrix (size 3x3)
%  - Number of landmarks
% 
% Outputs:
%  - Updated mean vector
%  - Updated covariance matrix
% 

function [mean_bar, err_bar] = EKF_Movement(mean_last, err_last, movement, R, N)

	% Type saving variable
	v  = movement(1);
	w  = movement(2);
	dt = movement(3);
	t_l = mean_last(3);
	
	% Movement model vectors
	if w == 0
		% FKM movement
		
		% Motion model vector (x,y,angle)
		move = [v*cos(t_l);
				v*sin(t_l);
				0];
		
		% Derivative of model vector wrt theta (x,y,angle)
		movd = [-v*sin(t_l);
				 v*cos(t_l);
				 0];
		
		% Multiply by time step to scale correctly
		move = move * dt;
		movd = movd * dt;
		
	else
		% ICR movement
		vrat = (v/w);
		
		% Motion model vector (x,y,angle)
		move = [-vrat*sin(t_l) + vrat*sin(t_l + w*dt);
				 vrat*cos(t_l) - vrat*cos(t_l + w*dt);
				 w * dt ];

		% Derivative of model vector wrt theta (x,y,angle)
		movd = [-vrat*cos(t_l) + vrat*cos(t_l + w*dt);
				-vrat*sin(t_l) + vrat*sin(t_l + w*dt);
				0 ];
	end

	
	% Initialize helper matrix
	Fx = [eye(3), zeros(3, 3*N)];
	
	% Adjusting mean from movement
	mean_bar = mean_last + ( Fx' * move );
	
	% Make angle be [0,2pi]
	mean_bar(3) = mod(mean_bar(3)+2*pi, 2*pi);
	
	% Matrix for error scaling due to movement
	Gt = eye(3*N + 3) + ( Fx' * [zeros(3,2), movd] * Fx );

	% Adjust error from movement and movement sensors
	err_bar = (Gt * err_last * Gt') + (Fx' * R * Fx);
	
end
