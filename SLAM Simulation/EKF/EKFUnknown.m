%% EKF SLAM with Unknown Correspondences
% Author: Connor Morales
% 
% Description:
%   This function performs one iteration of EKF SLAM with unknown
%   correspondences.  It is capable of using both ICR and FKM movement models
%   and will detect which one is needed based on input velocities.  For sizings
%   of the inputs, the number of landmarks in the map is denoted N.  If a
%   landmark is encountered that does not exist in the map, it will be added.
% 
% Inputs:
%  - Current mean value vector (size 3N+3 x 1)
%  - Current covariance matrix (size 3N+3 x 3N+3)
%  - Control action (linear vel, rotational vel, time step)
%  - Features measured (size 3xk, k>=0) (col: dist, angle, ID)
% 
% Outputs:
%  - New mean vector
%  - New covariance matrix
% 

function [mean_new, err_new, N_new] = EKFUnknown(mean_last, err_last, movement, features)

	% Get number of landmarks from mean
	N_new = floor((length(mean_last)-3) / 3);
	
	% Arbitrary small value
	alpha = 15;
	
	% Arbitrary big number
	big = 1E6;
	
	% Get sensor variance matrices
	[R,Q] = err_vals();

	% Perform movement update
	[mean_bar, err_bar] = EKF_Movement(mean_last, err_last, movement, R, N_new);
	
	
	% For all measurements
	for i = 1 : size(features, 2)
		
		% Add to N
		N_new = N_new + 1;
		
		% Observed feature for this loop (shift angle)
		z_i = features(:,i);
		z_i(2) = mod(z_i(2)+2*pi, 2*pi);
		
		% Convert measurement to cartesian, adjust for robot position
		init = [z_i(1)*cos(z_i(2) + mean_bar(3));
				z_i(1)*sin(z_i(2) + mean_bar(3));
				0];
		
		% Add new feature to mean
		mean_temp = [mean_bar(1:2);z_i(3)] + init;
		mean_bar = [mean_bar;mean_temp];
		
		% Add new blank feature to err
		err_bar = [err_bar, zeros(3*N_new,3); zeros(3,3*N_new), big*eye(3)];
		
		% Initialize saved H,S,z to large value
		P_small = alpha;
		H_small = big*big;
		S_small = big*big;
		z_hat_small = big*big;
		
		% For all features (including new one)
		for k = 1 : N_new
			
			% Helper distance vector
			delta = [mean_bar(k*3 + 1) - mean_bar(1);
					 mean_bar(k*3 + 2) - mean_bar(2)];
			
			% Helper distance variable
			q = delta' * delta;
			
			% Estimate for location (shift angle)
			z_hat = [sqrt(q) ;
					 atan2(delta(2), delta(1)) - mean_bar(3) ;
					 mean_bar(k*3 + 3) ];
			z_hat(2) = mod(z_hat(2)+2*pi, 2*pi);
			
			% Oh great, another F helper matrix
			Fxk = [eye(3),     zeros(3, 3*k-3), zeros(3,3), zeros(3, 3*N_new - 3*k);
				   zeros(3,3), zeros(3, 3*k-3), eye(3),     zeros(3, 3*N_new - 3*k)];
			
			% Let's make a Jacobian
			H = [-sqrt(q)*delta(1), -sqrt(q)*delta(2),  0, sqrt(q)*delta(1), sqrt(q)*delta(2), 0;
						  delta(2),         -delta(1), -q,        -delta(2),         delta(1), 0;
								 0,                 0,  0,                0,                0, q];
			H = (1/q)* H * Fxk;
			
			% Sensor error
			S = (H * err_bar * H') + Q;
			
			% Innovation w/ shifted angle
			inn = z_i - z_hat;
			inn(2) = mod(inn(2)+pi, 2*pi) - pi; % This was sooo painful to debug
			
			% Mahalanobis distance
			if k == N_new
				P = alpha;
			else
				P = inn' * inv(S) * inn;
			end
			
			% Save smallest value
			if P <= P_small
				P_small = P;
				H_small = H;
				S_small = S;
				z_hat_small = z_hat;
			end
			
		end
		
		% Check if we need to keep our added landmark
		if P_small == alpha
			fprintf('Added Landmark #%i (%i)\n', N_new, z_i(3));
		else
			
			% Reduce N back to previous value
			N_new = N_new - 1;
			
			% Shrink mean and err back down
			mean_bar = mean_bar(1:N_new*3+3);
			err_bar = err_bar(1:N_new*3+3, 1:N_new*3+3);
			
			% Shrink H down
			H_small = H_small(:, 1:N_new*3+3);
		end
		
		% Adjust our mean
		% K = err_bar * H_small' * inv( S_small );
		K = (err_bar * H_small') / ( S_small );
		
		% Innovation w/ shifted angle
		inn = z_i - z_hat_small;
		inn(2) = mod(inn(2)+pi, 2*pi) - pi; % This was sooo painful to debug
		
		% Adjust our mean
		mean_bar = mean_bar + ( K * inn );
		
		% Adjust our error
		err_bar = (eye(size(err_bar)) - K*H_small) * err_bar;
		
	end
	
	
	
	
	% Assign final mean and error
	mean_new = mean_bar;
	err_new  = err_bar;

end

