%% EKF SLAM with Known Correspondences
% Author: Connor Morales
% 
% Description:
%   This function performs one iteration of EKF SLAM with known correspondences.
%   It is capable of using both ICR and FKM movement models and will detect
%   which one is needed based on input velocities.  For sizings of the inputs,
%   the number of landmarks in the map is denoted N.
% 
% Inputs:
%  - Current mean value vector (size 3N+3 x 1)
%  - Current covariance matrix (size 3N+3 x 3N+3)
%  - Control action (linear vel, rotational vel, time step)
%  - Features measured (size 3xk, k>=0) (col: dist, angle, ID)
%  - Unique landmark IDs (size N x 1)
% 
% Outputs:
%  - New mean vector
%  - New covariance matrix
% 

function [mean_new, err_new] = EKFKnown(mean_last, err_last, movement, features, landmarks)

	% Get number of landmarks from mean
	N = floor((length(mean_last)-3) / 3);
	
	% Get sensor variance matrices
	[R,Q] = err_vals();
	
	
	% Perform movement update
	[mean_bar, err_bar] = EKF_Movement(mean_last, err_last, movement, R, N);
	
	
	% For all observed features
	for i = 1 : size(features, 2)
		
		% Observed feature for this loop (shift angle)
		z_i = features(:,i);
		z_i(2) = mod(z_i(2)+2*pi, 2*pi);
		
		% Find landmark j associated with measurement i (c_t^i)
		j = find(landmarks == features(3,i));
		
		% Check if never seen before
		if mean_bar(j*3 + 3) == 0
			
			% Convert to cartesian, adjust for robot rotation
			init = [z_i(1)*cos(z_i(2) + mean_bar(3));
					z_i(1)*sin(z_i(2) + mean_bar(3));
					0];
			
			% Assign initial mean (position)
			mean_bar(j*3+1 : j*3+3) = [mean_bar(1:2);j] + init;
			
		end
		
		% Helper vector
		delta = [mean_bar(j*3 + 1) - mean_bar(1);
				 mean_bar(j*3 + 2) - mean_bar(2)];
		
		% Another helper variable
		q = delta' * delta;
		
		% Estimate for location, shift angle
		z_hat = [sqrt(q) ;
				 atan2(delta(2), delta(1)) - mean_bar(3) ;
				 mean_bar(j*3 + 3) ];
		z_hat(2) = mod(z_hat(2)+2*pi, 2*pi);
		
		% Oh great, another F helper matrix
		Fxj = [eye(3),     zeros(3, 3*j-3), zeros(3,3), zeros(3, 3*N - 3*j);
			   zeros(3,3), zeros(3, 3*j-3), eye(3),     zeros(3, 3*N - 3*j)];
		
		% Let's make a Jacobian
		H = [-sqrt(q)*delta(1), -sqrt(q)*delta(2),  0, sqrt(q)*delta(1), sqrt(q)*delta(2), 0;
					  delta(2),         -delta(1), -q,        -delta(2),         delta(1), 0;
							 0,                 0,  0,                0,                0, q];
		H = (1/q)* H * Fxj;
		
		% Innovation w/ shifted angle
		inn = z_i - z_hat;
		inn(2) = mod(inn(2)+pi, 2*pi) - pi; % This was sooo painful to debug
		
		% Adjust our mean
		% K = err_bar * H' * inv( (H * err_bar * H') + Q );
		K = (err_bar * H') / ( (H * err_bar * H') + Q );
		mean_bar = mean_bar + ( K * inn );
		
		% Adjust our error
		err_bar = (eye(size(err_bar)) - K*H) * err_bar;
		
	end
	
	
	% Assign final mean and error
	mean_new = mean_bar;
	err_new  = err_bar;

end

