%% Robot Error Values
% Author: Connor Morales
% 
% Description:
%   This function computes the sensor error covariance matrices.
%   These values can be edited as needed to match the simulation environment.
% 

function [R,Q] = err_vals()

	% Movement sensor variance
	varX = (0.1) ^ 2;
	varY = (0.1) ^ 2;
	varT = (0.05) ^ 2;
	
	% Landmark measurement sensor variance
	varR = (0.1) ^ 2;
	varP = (0.1) ^ 2;
	varS = (1.0) ^ 2; % NOTE: cannot be 0
	
	
	
	
	
	% Movement sensor covariance matrix
	R = [varX, 0,    0;
		 0,    varY, 0;
		 0,    0,    varT];
	
	% Measurement sensor covariance matrix
	Q = [varR, 0,    0;
		 0,    varP, 0;
		 0,    0,    varS];
	
end
