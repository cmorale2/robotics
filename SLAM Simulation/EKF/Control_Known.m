%% Control Function  - EKF SLAM with Known Correspondences
% Author: Connor Morales
% 

function [] = Control_Known(robotObj)

	% Map constants
	[~, landmarks, N, ~] = map_consts();
	
	% Mean and Covariance initialization
	[mean_pos, err_pos] = init_control(N);

	% Control action variables
	v = 0; % [m/s]
	w = 0; % [m/s]
	dt = 0.1;  % [s]
	
	% Set initial robot location
	setMapStart(robotObj, [mean_pos(1), mean_pos(2), mean_pos(3)]);
	
	% Start robot off not moving
	SetFwdVelAngVel(robotObj, 0, 0);
	
	% Intial SLAM
	[mean_pos, err_pos] = EKFKnown(mean_pos, err_pos, [v,w,dt], [], landmarks);
	
	% Figure for drawing
	fig = figure('Name','Mean and Covariance');
	cnt = 0;
	cmax = 5;
	
	% Enter main loop
	while true
		
		% Perform control action for specified length of time, then pause and
		% wait for calculations to complete
		[v, w] = move_control(robotObj);
		SetFwdVelAngVel(robotObj, v, w);
		pause(dt)
		SetFwdVelAngVel(robotObj, 0, 0);
		
		
		% Read beacon sensor
		[x, y, n] = ReadBeacon(robotObj);
		
		% Check if we have a measurement
		if ~isempty(x)
			% Fill feature matrix with measurements
			features = zeros(3, length(x));
			for i = 1 : length(x)
				features(1,i) = sqrt(x(i)^2 + y(i)^2);
				features(2,i) = atan2(y(i), x(i));
				features(3,i) = n(i);
			end
		else
			% No measurements to report
			features = [];
		end
		
		
		% Perform SLAM
		[mean_pos, err_pos] = EKFKnown(mean_pos, err_pos, [v,w,dt], features, landmarks);

		
		
		% Update plot if our counter hits its max value
		if cnt >= cmax
			plotCovariance(fig, mean_pos, err_pos, robotObj);
			cnt = 0;
		else
			cnt = cnt + 1;
		end
		
		% Quit if the robot has stopped
		% Update plot if it was not just updated
		if ~robotObj.autoCheck
			if cnt ~= 0
				plotCovariance(fig, mean_pos, err_pos, robotObj);
			end
			break;
		end
		
	end
	
end

