%% Robot Control Initialization
% Author: Connor Morales
% 
% Description:
%   This function is used to initialize the mean and covariance matrices for the
%   simulation.
%   If the starting location of the robot needs to be changed, do it here.
% 

function [mean, cov] = init_control(N)

	% Large initial landmark covariance value
	bval = 1E6;

	% Initialize mean position matrix
	mean = zeros(3*N+3, 1);
	mean(1) =  2.0; % x
	mean(2) = -2.0; % y
	mean(3) =  2.0; % angle
	
	% Initialize error matrix
	cov = [zeros(3,3),    zeros(3, 3*N);
	       zeros(3*N, 3), bval*eye(3*N, 3*N)];
	
end
