%% Helper Function - Plotting Covariance
% Author: Connor Morales
% 
% Description:
%    This function displays a plot of the robot and obstacles on the map within
%    the limits specified in the 'map_consts' function.  The pose input is
%    assumed to be the 'true' position of the robot.  The mean and error are
%    used to draw estimates with error ellipses.
% 
% Inputs:
%   - Figure to draw to, will be cleared
%   - Mean position estimates
%   - Covariance matrix of estimates
%   - Robot object
% 
% Outputs:
% 

function [] = plotCovariance(fig, mean, err, robotObj)

	% Grab map info we need
	[beacons, ~, ~, lims] = map_consts();
	
	% Get number of landmarks from mean
	N = floor((length(mean)-3) / 3);
	
	% Get true position
	[x, y, t] = OverheadLocalization(robotObj);
	t = mod(t, 2*pi);
	
	% Setup figure
	figure(fig)
	cla
	
	% Actual robot location
	plot(x,y, 'k+');
	hold on
	
	% Actual beacon locations
	scatter(beacons(:,1), beacons(:,2), 'kx');

	
	% Estimate of robot position with uncertainty
	e = covarianceEllipse(mean(1:2), err(1:2,1:2));
	plot(mean(1), mean(2), 'b+');
	plot(e(:,1), e(:,2), 'b-');
	
	
	% Estimate of landmark position with uncertainty
	for i = 1 : N
		% Grab subsection of larger mean/covariance for this landmark
		mean_temp = mean(i*3+1 : i*3+3);
		err_temp = err(i*3+1:i*3+2, i*3+1:i*3+2);

		% Skip drawing this one if hasn't been seen yet
		if mean_temp(3) == 0
			continue;
		end
		
		% If we are drawing, set our flag
		flag = true;

		% Actual plotting
		e = covarianceEllipse(mean_temp, err_temp);
		plot(mean_temp(1), mean_temp(2), 'r+');
		plot(e(:,1), e(:,2), 'r-');
	end
	
	% Actual angle robot is facing
	line([x, x+cos(t)*0.5], [y, y+sin(t)*0.5],  'Color', 'k');
	
	% Estimate of angle robot is facing
	line([mean(1), mean(1)+cos(mean(3))*0.5], [mean(2), mean(2)+sin(mean(3))*0.5],  'Color', 'b');

	% Set limits of plot
	xlim([lims(1,1)-1, lims(1,2)+1]);
	ylim([lims(2,1)-1, lims(2,2)+1]);
	
	% Draw boundary walls
	rectangle('Position', [lims(1,1), lims(2,1), lims(1,2)-lims(1,1), lims(2,2)-lims(2,1)]);
	
	% Legend depends on if we've drawn landmarks or not
	if N > 0
		legend('Actual Robot', 'Actual Beacons', 'Robot Mean', 'Robot Covariance', 'Beacon Mean', 'Beacon Covariance', 'Location', 'eastoutside');
	else
		legend('Actual Robot', 'Actual Beacons', 'Robot Mean', 'Robot Covariance', 'Location', 'eastoutside');
	end
	
	title('Mean and Covariance');
end





%% Ellipse from Covariance Matrix
% Author: Connor Morales
% 
% Description:
%    This function takes a mean vector and covariance matrix and produces an
%    ellipse representing the covariance centered around the mean value.
% 
% Inputs:
%   - Mean of object (x,y)
%   - Covariance of object (size 2x2)
% 
% Outputs:
%   - Ellipse (size Nx2)
% 

function [e] = covarianceEllipse(mean, err)

	% Get eigen vectors/values of x,y covariance
	[V, D] = eig(err);
	D = diag(D);
	
	% Sort by eigenvalues
	[D, ind] = sort(D, 'descend');
	V = V(:, ind);
	
	% Initialize unit circle grid
	theta = linspace(0,2*pi, 51)';
	
	% Generate unit circle
	e = [cos(theta), sin(theta)];
	
	% Scale and rotate unit circle into ellipse
	e = e * (V*sqrt(diag(D)));
	
	% Add mean offset
	e(:,1) = e(:,1) + mean(1);
	e(:,2) = e(:,2) + mean(2);
	
end

