%% Map Constants
% Author: Connor Morales
% 
% Description:
%   This function is used to tell the control program the map configuration.  It
%   should always match the map used in simulation.
%   The i^th row of 'beacons' should correspond to the i^th element in
%   'landmarks'.  All beacons specified in the map should be redeclared in this
%   function.
%   The limits of the map should also be specified in 'lims'.
% 

function [beacons, landmarks, N, lims] = map_consts()

	% Beacon locations
	beacons = [
		-3.5  3.5 ;
		 3.5  3.5 ;
		-3.5 -3.5 ;
		 3.5 -3.5 ;
		-3.5 -2.0 ;
		-3.5  0.0 ;
		-3.5  2.0 ;
		 3.5 -2.0 ;
		 3.5  0.0 ;
		 3.5  2.0 ;
		-2.0  3.5 ;
		 0.0  3.5 ;
		 2.0  3.5 ;
		-2.0 -3.5 ;
		 0.0 -3.5 ;
		 2.0 -3.5 ;
		-2.5 -2.5 ;
		-2.5  2.5 ;
		 2.5 -2.5 ;
		 2.5  2.5 ;
		-2.5  0.0 ;
		 2.5  0.0 ;
		 0.0 -2.5 ;
		 0.0  2.5 ;
		-1.0  1.0 ;
		-1.0 -1.0 ;
		 1.0  1.0 ;
		 1.0 -1.0 
	];
	
	% Beacon markers (same order as 'beacons' var)
	landmarks = (1:28)';
	
	% Number of landmarks
	if length(landmarks) ~= size(beacons,1)
		error('The number of beacons must be the same as the number of landmarks');
	end
	N  = length(landmarks);
	
	% Map limits
	lims = [-4, 4 ;  % x
	        -4, 4 ]; % y
	
end
