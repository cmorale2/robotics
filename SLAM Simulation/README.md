# Robotics - SLAM Simulation
## Connor Morales

***
This directory contains the simulation environment for SLAM as well as various implementations.

The simulation environment is an edited version of the one developed by Cornell University.

This simulation environment has been tested in MATLAB R2015b and later in both Windows and Linux operating systems.

The SLAM algorithms were developed and tested in MATLAB R2017a on Ubuntu Linux.

***

To run the simulator, open this directory in MATLAB.  Run the following command:
```
SimulatorGUI
```

The simulator window should open.  Click the "Load Map" button, and select a map text file from the SLAM variation subdirectory desired.

Click the "Start" button to select a simulation script.  A "Control_*.m" script should be chosen from the same subdirectory the map was taken from.

Additional figures will be produced by the algorithm.


*** 

The default CreateRobot class has no sensor errors.  Backups of this class both with and without error are included in the repo (CreateRobot.m.err, CreateRobot.m.noerr).  To change to simulation to include sensor error, simply rename these files.  If this is done, the simulator GUI must be closed and reopened.

When using a CreateRobot class void of errors, the EKF variants perform almost perfectly.  The Fast variants do not; sometimes, their error can be on the order of ~1/2 meter.

Using the CreateRobot class with error, all variants have the possibility of not converging to the correct poses, or even diverging.  EKF with correspondences usually performs well, but the pose estimates have a habit of drifting form their true locations.  When this happens, it is usually consistent over all poses.  EKF without correspondences sometimes completely diverges into a large number of landmarks, but usually has large drifts in pose estimates.  FastSLAM drifts a lot, in inconsistent directions across landmarks.


***
