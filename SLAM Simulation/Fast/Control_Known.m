%% Control Function - FastSLAM with Known Correspondences
% Author: Connor Morales




function [] = Control_Known(robotObj)

	% Map constants
	[~, landmarks, ~, ~] = map_consts();
	
	% Initialize SLAM
	particles = FastSLAM_Init();
	
	% Control action variables
	v = 0; % [m/s]
	w = 0; % [m/s]
	dt = 0.1;  % [s]
	
	% Set initial robot location
	setMapStart(robotObj, init_control());
	
	% Start robot off not moving
	SetFwdVelAngVel(robotObj, 0, 0);
	
	% Initial SLAM
	particles = FastSLAM(particles, [v,w,dt], [], landmarks);
	
	
	% Particle cloud figure
	f = figure('Name','Particle Clouds');
	cnt = 0;
	cmax = 5;
	
	
	% Enter main loop
	while true
		
		% Perform control action for specified length of time, then pause and
		% wait for calculations to complete
		[v, w] = move_control(robotObj);
		SetFwdVelAngVel(robotObj, v, w);
		pause(dt)
		SetFwdVelAngVel(robotObj, 0, 0);
		
		
		% Read beacon sensor
		[x, y, n] = ReadBeacon(robotObj);
		
		% Check if we have a measurement
		if ~isempty(x)
			% Fill feature matrix with measurements
			features = zeros(3, length(x));
			for i = 1 : length(x)
				features(1,i) = sqrt(x(i)^2 + y(i)^2);
				features(2,i) = atan2(y(i), x(i));
				features(3,i) = n(i);
			end
		else
			% No measurements to report
			features = [];
		end
		
		
		
		
		% Perform SLAM
		particles = FastSLAM(particles, [v,w,dt], features, landmarks);

		
		
		% Update plot if our counter hits its max value
		if cnt >= cmax
			plotClouds(f, particles, robotObj);
			cnt = 0;
		else
			cnt = cnt + 1;
		end
		
		% Quit if the robot has stopped
		% Update plot if it was not just updated
		if ~robotObj.autoCheck
			if cnt ~= 0
				plotClouds(f, particles, robotObj);
			end
			break;
		end
		
	end
	
	
end




