%% Helper Function - Plotting Covariance
% Author: Connor Morales
% 
% Description:
%    This function displays a plot of the robot and obstacles on the map within
%    the limits specified in the 'map_consts' function.  The pose input is
%    assumed to be the 'true' position of the robot.  The mean and error are
%    used to draw estimates with error ellipses.
% 
% Inputs:
%   - Figure to draw to, will be cleared
%   - Mean position estimates
%   - Covariance matrix of estimates
%   - Robot object
% 
% Outputs:
% 

function [] = plotClouds(fig, particles, robotObj)

	% Grab map info we need
	[beacons, ~, N, lims] = map_consts();
	
	% Number of particles
	M = length(particles);
	
	% Get true position
	[x, y, t] = OverheadLocalization(robotObj);
	t = mod(t, 2*pi);
	
	
	% Set up robot pose vars
	posePts = zeros(2,M);
	poseAng = zeros(1,M);
	
	% Get points for robot pose
	for k = 1 : M
		posePts(:,k) = particles{k}.Pose(1:2);
		poseAng(k) = particles{k}.Pose(3);
	end
	
	% Calculate mean/covariance
	poseMean = mean(posePts, 2);
	
	% Calculate angle average/std
	angMean = mean(poseAng);
	angStd = std(poseAng);
	
	
	
	
	% Landmark plot flag
	flag = false;
	
	% Set up cell array for points at each landmark
	landPts = cell(N,1);
	
	% Populate scatter points at each landmark
	for n = 1 : N
		
		% Nothing to do if landmark not seen yet
		if particles{1}.Map{n}.Mark == 0
			continue;
		end
		
		% Mark our landmark flag as true
		flag = true;
		
		% Initialize (x,y) matrix
		landPts{n} = zeros(2, M);
		
		% Populate indices for each particle
		for k = 1 : M
			landPts{n}(:,k) = particles{k}.Map{n}.Pose;
		end
		
	end
	
	
	
	
	% Setup figure
	figure(fig)
	cla
	
	% Actual robot/beacon locations
	plot(x,y, 'k+');
	hold on
	scatter(beacons(:,1), beacons(:,2), 'kx');
	
	
	% Plot robot pose estimates
	scatter(posePts(1,:), posePts(2,:), 'b.');
	
	% Plot landmark pose estimates
	for n = 1 : N
		if particles{1}.Map{n}.Mark ~= 0
			scatter(landPts{n}(1,:), landPts{n}(2,:), 'r.');
		end
	end
	
	% Plot robot angle mean and std
	line([poseMean(1), poseMean(1)+cos(angMean)], [poseMean(2), poseMean(2)+sin(angMean)], 'Color','b');
	line([poseMean(1), poseMean(1)+cos(angMean-angStd)], [poseMean(2), poseMean(2)+sin(angMean-angStd)], 'Color','b', 'LineStyle','--');
	line([poseMean(1), poseMean(1)+cos(angMean+angStd)], [poseMean(2), poseMean(2)+sin(angMean+angStd)], 'Color','b', 'LineStyle','--');
	
	% Plot actual robot angle
	line([x, x+cos(t)], [y, y+sin(t)], 'Color','k');
	
	
	% Axis limits
	xlim(lims(1,:));
	ylim(lims(2,:));
	
	% Labeling
	if flag
		legend('Actual Robot','Actual Beacons','Robot Estimate','Beacon Estimate', 'Location','eastoutside');
	else
		legend('Actual Robot','Actual Beacons','Robot Estimate', 'Location','eastoutside');
	end
	
	title('Particle Clouds');
	
end





%% Ellipse from Covariance Matrix
% Author: Connor Morales
% 
% Description:
%    This function takes a mean vector and covariance matrix and produces an
%    ellipse representing the covariance centered around the mean value.
% 
% Inputs:
%   - Mean of object (x,y)
%   - Covariance of object (size 2x2)
% 
% Outputs:
%   - Ellipse (size Nx2)
% 

function [e] = covarianceEllipse(mean, err)

	% Get eigen vectors/values of x,y covariance
	[V, D] = eig(err);
	D = diag(D);
	
	% Sort by eigenvalues
	[D, ind] = sort(D, 'descend');
	V = V(:, ind);
	
	% Initialize unit circle grid
	theta = linspace(0,2*pi, 11)';
	
	% Generate unit circle
	e = [cos(theta), sin(theta)];
	
	% Scale and rotate unit circle into ellipse
	e = e * (V*sqrt(diag(D)));
	
	% Add mean offset
	e(:,1) = e(:,1) + mean(1);
	e(:,2) = e(:,2) + mean(2);
	
end

