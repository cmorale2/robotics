%% Movement Control
% Author: Connor Morales
% 
% Description:
%   This function is the movement control function for the control program.  It
%   is incredibly basic, as it is meant for simple SLAM tests, not for path
%   planning.
% 

function [v, w] = move_control(robotObj)
	
	% Get robot position
	[x, y, t] = OverheadLocalization(robotObj);
	t = mod(t, 2*pi);
	
	vval = 0.1;
	wval = 0.4;
	vmax = 0.45;
	wmin = 0.05;
	
	% Pick 
	if x <= -2.5 % If too far left, turn right
			
		v = vval;
		if t > 0 && t < pi
			w = -wval;
		else
			w = wval;
		end

	elseif x >= 2.5 % If too far right, turn left

		v = vval;
		if t > 0 && t < pi
			w = wval;
		else
			w = -wval;
		end

	elseif y >= 2.5 % If we are too high, turn down

		v = vval;
		if t > pi/2 && t < 3*pi/2
			w = wval;
		else
			w = -wval;
		end

	elseif y <= -2.5 % If we are too low, turn up

		v = vval;
		if t > pi/2 && t < 3*pi/2
			w = -wval;
		else
			w = wval;
		end

	else
		
		v = vmax;
		if rand(1) > 0.5
			w = mod( rand(1), 2*wmin ) - wmin;
		else
			w = 0;
		end
		
	end
	
end





function w = v2w(v)
% Calculate the maximum allowable angular velocity from the linear velocity
%
% Input:
% v - Forward velocity of Create (m/s)
%
% Output:
% w - Angular velocity of Create (rad/s)
	% Robot constants
	maxWheelVel= 0.5;   % Max linear velocity of each drive wheel (m/s)
	robotRadius= 0.08;   % Radius of the robot (m)

	% Max velocity combinations obey rule v+wr <= v_max
	w= (maxWheelVel-v)/robotRadius;
end
