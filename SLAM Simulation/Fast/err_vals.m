%% Robot Error Values
% Author: Connor Morales
% 
% Description:
%   This function computes the sensor error covariance matrices.
%   These values can be edited as needed to match the simulation environment.
% 

function [Q,a] = err_vals()

	% Landmark measurement sensor variance
	varR = (0.1) ^ 2;
	varP = (0.1) ^ 2;
	
	
	% Movement error values
% 	a = zeros(3,1);
	a = [
		0.05; % x
		0.05; % y
		0.03; % angle
	];
	
	
	
	% Measurement sensor covariance matrix
	Q = [varR, 0 ;
		 0,    varP ];
	
end
