%% FastSLAM - Initialize Particles
% Author: Connor Morales
% 
% Description:
%   This function initializes particles for FastSLAM.  The number of particles
%   should be adjusted in this function (tested @ 25).
% 
% Inputs:
% 
% Outputs:
%  - Cell array of particles
% 

function [particles] = FastSLAM_Init()

	% Number of particles
	num = 100;

	% Arbitrary large value
	bval = 1E6;

	% Get map information
	[~, ~, N, ~] = map_consts();
	
	% Get initial robot pose
	pose = init_control();
	
	% Initialize particle cell array
	particles = cell(num, 1);
	for k = 1 : num
		
		particles{k} = struct;
		
		% Initial pose is completely accurate
		particles{k}.Pose = pose;
		
		% Map is cell array of landmarks
		particles{k}.Map = cell(N,1);
		for l = 1 : N
			particles{k}.Map{l} = struct;
			
			% Pose is @ (0,0)
			particles{k}.Map{l}.Pose = zeros(2,1);
			% Set landmark identifier to blank (unseen)
			particles{k}.Map{l}.Mark = 0;
			% (Near) infinite initial variance
			particles{k}.Map{l}.Err = bval*eye(2);
		end
		
	end

end

