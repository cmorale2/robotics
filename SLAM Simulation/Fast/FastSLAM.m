%% FastSLAM with Known Correspondences
% Author: Connor Morales
% 
% Description:
%   This function performs one iteration of FastSLAM with known correspondences.
%   It is capable of using both ICR and FKM movement models and will detect
%   which one is needed baed on input velocities.  The particles input should
%   have the form specified in the initialization function.
% 
% Inputs:
%  - Cell array of particles (see FastSLAM_Init.m)
%  - Control action (linear vel, rotational vel, time step)
%  - Features measured (size 3xk, k>=0) (col: dist, angle, ID)
%  - Unique landmark IDs (size N x 1)
% 
% Outputs:
%  - Updated cell array of particles, same size as input
% 

function [particles] = FastSLAM(particles, movement, features, landmarks)

	% Number of particles
	M = length(particles);
	
	% Get sensor variance matrices
	[Q,~] = err_vals();

	% Initialize weights
	w = ones(M,1);
	
	% Default weight
	p0 = 1;

	
	% Go through all particles
	for k = 1 : M
		
		% Typesaving
		Y = particles{k};
		
		% Sample pose to perform movement update
		Y.Pose = FastSLAM_MovementUpdate(Y.Pose, movement);
		
		
		% Go through all measurements
		for i = 1 : size(features, 2)
			
			% Typesaving (and shifting angle)
			z_i = features(1:2,i);
			z_i(2) = mod(z_i(2)+2*pi, 2*pi);
			
			% Determine which landmark we are looking at
			j = find(landmarks == features(3,i));
			
			% Determine if we haven't seen this before
			nsbf = Y.Map{j}.Mark ~= landmarks(j);
			
			% Initialize position if we haven't seen it before
			if nsbf
				% Convert polar to cartesian and shift by robot pose to global
				% coordinates
				Y.Map{j}.Pose = [
					z_i(1)*cos(z_i(2) + Y.Pose(3)) + Y.Pose(1) ;
					z_i(1)*sin(z_i(2) + Y.Pose(3)) + Y.Pose(2) ;
				];
				Y.Map{j}.Mark = landmarks(j);
			end
			
			% Helper vector
			delta = [Y.Map{j}.Pose(1) - Y.Pose(1);
					 Y.Map{j}.Pose(2) - Y.Pose(2)];

			% Another helper variable
			q = delta' * delta;
			
			% Let's make a Jacobian
			H = [
				sqrt(q)*delta(1), sqrt(q)*delta(2) ;
				-delta(2),        delta(1)
			];
			H = H / q;
			
			
			
			% Update uncertainty directly if this is first time seeing landmark
			if nsbf
				Y.Map{j}.Err = inv(H) * Q * inv(H)';
				w(k) = w(k) * p0;
				continue;
			end
			
			
			% If we have already seen this landmark, update as normal
			
			% Error matrix
			S = H * Y.Map{j}.Err * H' + Q;
			Sinv = inv(S);
			
			% Kalman gain
			K = Y.Map{j}.Err * H' * Sinv;
			
			% Estimate for location (shift angle to be [0,2pi])
			z_hat = [
				sqrt(q) ;
				atan2(delta(2), delta(1)) - Y.Pose(3)
			];
			z_hat(2) = mod(z_hat(2)+2*pi, 2*pi);
			
			
			% Innovation
			inn = (z_i - z_hat);
			inn(2) = mod(inn(2)+pi, 2*pi) - pi; % This was sooo painful to debug
			
			% Landmark pose update
			Y.Map{j}.Pose = Y.Map{j}.Pose + K*inn;
			
			% Landmark uncertainty update
			Y.Map{j}.Err = (eye(2) - K*H) * Y.Map{j}.Err;
			
			% Update particle weight
			w(k) = w(k) * det(2*pi*S)^(-0.5) * exp(-0.5 * inn'*Sinv*inn);
			
		end

		
		
		% Assign particle back to list
		particles{k} = Y;
		
		
		
	end

	
	
	% Select new particles based on weight
	p_new = cell(M,1);
	
	% Create edges for bins
	e = [0; cumsum(w/sum(w))];
	
	% Sort random numbers into bins specified by particle weights
	[~,~,ind] = histcounts(rand(M,1), e);
	
	% We want same # of particles
	for k = 1 : M
		p_new{k} = particles{ind(k)};
	end
	
	% Assign new particles
	particles = p_new;
	

end










