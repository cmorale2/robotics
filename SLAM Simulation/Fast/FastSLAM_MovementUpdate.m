%% FastSLAM - Movement Update
% Author: Connor Morales
% 
% Description:
%   This function performs the movement update for FastSLAM.  It is capable of
%   using both ICR and FKM movement models and will detect which one is needed
%   based on input velocities.
%   The pose is sampled by adding Gaussian noise with standard deviation
%   adjusted by error values from err_vals.m
% 
% Inputs:
%   - Robot pose (x,y,theta)
%   - Control action (linear vel, rotational vel, time step)
% 
% Outputs:
%  - Sampled robot pose (same elements as input)
% 

function [pose] = FastSLAM_MovementUpdate(pose, movement)

	% Error constants
	[~,a] = err_vals();


	% Movement typesaving
	v = movement(1);
	w = movement(2);
	dt = movement(3);
	
	% Pose typesaving
	x = pose(1);
	y = pose(2);
	t = pose(3);
	
	% Calculate new pose
	if w == 0 % FKM
		x = x + dt*v*cos(t);
		y = y + dt*v*sin(t);
		
	else % ICR
		vrat = v/w;
		x = x - vrat*sin(t) + vrat*sin(t + w*dt);
		y = y + vrat*cos(t) - vrat*cos(t + w*dt);
		t = t + w*dt;
		
	end
	
	% Assign output mose
	pose = [x;y;t];
	
	% Add error to pose
	pose = pose + a.*randn(3,1);
	
	% Adjust angle
	pose(3) = mod(pose(3)+2*pi, 2*pi);
	
end
