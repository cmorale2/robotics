%% Robot Control Initialization
% Author: Connor Morales
% 
% Description:
%   This function is used to initialize the initial pose of the robot
%   If the starting location of the robot needs to be changed, do it here.
% 

function [pose] = init_control()

	% Initialize mean position matrix
	pose = zeros(3, 1);
	pose(1) =  1.0; % x
	pose(2) = -2.0; % y
	pose(3) =  2.0; % angle
	
end
