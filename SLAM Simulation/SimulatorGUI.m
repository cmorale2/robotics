function varargout = SimulatorGUI(varargin)
% SimulatorGUI M-file for SimulatorGUI.fig
% Simulator for the iRobot Create
% To run, enter "SimulatorGUI" into the command window with not input or
%   output arguments.  This will bring up the GUI window for the simulator,
%   and initialize a CreateRobot object to hold simulation data and perform
%   the required functions.

% SimulatorGUI.m
% Copyright (C) 2011 Cornell University
% This code is released under the open-source BSD license.  A copy of this
% license should be provided with the software.  If not, email:
% CreateMatlabSim@gmail.com

% Edit the above text to modify the response to help SimulatorGUI

% Last Modified by GUIDE v2.5 25-Jan-2017 09:50:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SimulatorGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @SimulatorGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SimulatorGUI is made visible.
function SimulatorGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)
% UIWAIT makes SimulatorGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% Choose default command line output for SimulatorGUI
% Output all handles for use with updating
handles.output = handles;

% Store robot object for access to functions
% Store in UserData of title block because no where else to put it
obj= CreateRobot(handles);
set(handles.text_title,'UserData',obj)

% Get constant properties for use in plotting
[rad, rIR]= getConstants(obj);

% Plot robot in default position and store plot handles for updating
axes(handles.axes_map)
circ_numPts= 21;    % Estimate circle as circ_numPts-1 lines
circ_ang=linspace(0,2*pi,circ_numPts);
circ_rad=ones(1,circ_numPts)*rad;
[circ_x circ_y]= pol2cart(circ_ang,circ_rad);
handle_circ= plot(circ_x,circ_y,'b-','LineWidth',1.5);
handle_line= plot([0 1.5*rad],[0 0],'b-','LineWidth',1.5);
set(handles.figure_simulator,'UserData',[handle_circ ; handle_line])

% Plot initial sensors visualization and make invisible
% These plot coordinates are rough to reduce computation here, but it is
%   required that something is plotted
handle_irF= plot([rad rad+rIR],[0 0],'g-','LineWidth',2);
handle_irL= plot([0 0],[rad rad+rIR],'g-','LineWidth',2);
handle_irR= plot([0 0],[-rad -(rad+rIR)],'g-','LineWidth',2);
handle_bumpR= plot([0 rad],[-rad 0],'m-','LineWidth',2);
handle_bumpF= plot([rad rad],[-rad/2 rad/2],'m-','LineWidth',2);
handle_bumpL= plot([0 rad],[rad 0],'m-','LineWidth',2);
handles_sensors= [handle_irF handle_irL handle_irR handle_bumpR handle_bumpF handle_bumpL]';
set(handles_sensors,'Visible','off')
set(handles.axes_map,'UserData',handles_sensors)

% Set up timer to control simulation updates
timerSim= timer;
timerSim.BusyMode= 'drop';
timerSim.ExecutionMode= 'fixedSpacing';
%%%%%%%%%%Change?%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
timerSim.Name= 'CreateSim';    % To specify timer for deletion
timerSim.ObjectVisibility= 'on';
%%%%%%%%%%Change?%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
timerSim.Period= 0.1;
timerSim.TasksToExecute= inf;
timerSim.TimerFcn= {@updateSim,obj,handles};
start(timerSim)

% Add in axes toolbar
set(hObject,'toolbar','figure');

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = SimulatorGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close figure_simulator.
function figure_simulator_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure_simulator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Delete the timer to prevent further updates
timerList= timerfindall('Name','CreateSim');
if ~isempty(timerList)
    stop(timerList)
    delete(timerList)
end

% Use conditional in case figure is opened without running simulator
if ~isempty(handles)
    % Get robot object
    obj= get(handles.text_title,'UserData');
    
    % Disable autonomous control
    setAutoEnable(obj,false)
end

% Close the figure
delete(hObject);


% --- Executes on button press in push_map.
function push_map_Callback(hObject, eventdata, handles)
% hObject    handle to push_map (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Import map file
[filename pathname filter]= uigetfile('*.txt','Import Map File');
if filename                         % Make sure cancel was not pressed
    fid= fopen([pathname filename]);% Get file handle for parsing
    
    % Parse the file and extract relevant information
    walls= [];
    lines= [];
    beacs= {};
    vwalls= [];
    while ~feof(fid)
        fline= fgetl(fid);
        line= lower(fline);     % Convert to lowercase if necessary
        line= strtrim(line);    % Delete leading and trailing whitespace
        lineWords= {};          % To keep track of line entries
        while ~isempty(line) && ~strcmp(line(1),'%')% End of line or comment
            [word, line]= strtok(line);  % Get next entry
            line= strtrim(line);        % To be able to detect comments
            lineWords= [lineWords word];
        end
        
        %%% Regular expressions would probably be more efficient %%%
        if length(lineWords) == 5 && strcmp(lineWords{1},'wall') && ...
                ~isnan(str2double(lineWords{2})) && ...
                ~isnan(str2double(lineWords{3})) && ...
                ~isnan(str2double(lineWords{4})) && ...
                ~isnan(str2double(lineWords{5}))
            walls= [walls ; str2double(lineWords{2}) ...
                str2double(lineWords{3}) str2double(lineWords{4}) ...
                str2double(lineWords{5})];
        elseif length(lineWords) == 5 && strcmp(lineWords{1},'line') && ...
                ~isnan(str2double(lineWords{2})) && ...
                ~isnan(str2double(lineWords{3})) && ...
                ~isnan(str2double(lineWords{4})) && ...
                ~isnan(str2double(lineWords{5}))
            lines= [lines ; str2double(lineWords{2}) ...
                str2double(lineWords{3}) str2double(lineWords{4}) ...
                str2double(lineWords{5})];
        elseif length(lineWords) == 7 && strcmp(lineWords{1},'beacon') ...
                && ~isnan(str2double(lineWords{2})) && ...
                ~isnan(str2double(lineWords{3})) && ...
                length(lineWords{4}) >= 2 && ...
                strcmp(lineWords{4}(1),'[') && ...
                ~isnan(str2double(lineWords{4}(2:end))) && ...
                ~isnan(str2double(lineWords{5})) && ...
                length(lineWords{6}) >= 2 && ...
                strcmp(lineWords{6}(end),']') && ...
                ~isnan(str2double(lineWords{6}(1:end-1)))
            beacs= [beacs ; str2double(lineWords{2}) ...
                str2double(lineWords{3}) ...
                str2double(lineWords{4}(2:end)) ...
                str2double(lineWords{5}) ...
                str2double(lineWords{6}(1:end-1)) lineWords(7)];
        elseif length(lineWords) == 5 && ...
                strcmp(lineWords{1},'virtwall') && ...
                ~isnan(str2double(lineWords{2})) && ...
                ~isnan(str2double(lineWords{3})) && ...
                ~isnan(str2double(lineWords{4})) && ...
                ~isnan(str2double(lineWords{5})) && ...
                str2double(lineWords{5}) >= 1 && ...
                str2double(lineWords{5}) <= 3
            vwalls= [vwalls ; str2double(lineWords{2}) ...
                str2double(lineWords{3}) str2double(lineWords{4}) ...
                str2double(lineWords{5})];
        elseif ~isempty(lineWords)
            warning('MATLAB:invalidInput',...
                'This line in map file %s is unrecognized:\n\t%s',...
                filename,fline)
        end
    end
    fclose(fid);
    
    % Set map data in robot object
    obj= get(handles.text_title,'UserData');
    setMap(obj,walls,lines,beacs,vwalls)
    
    % Clear old map
    axes(handles.axes_map)
    children= get(gca,'Children');  % All ploted items on axes
    handles_sensors= get(handles.axes_map,'UserData');
    for i= 1:length(handles_sensors)    % Keep sensors visualization
        children(find(handles_sensors(i) == children))= [];
    end
    if ~isempty(children)
        delete(children(1:end-2))	% Delete all but robot from axes
    end
    
    % Plot walls
    for i= 1:size(walls,1)
        plot(walls(i,[1 3]),walls(i,[2 4]),'k-','LineWidth',1)
    end
    
    % Plot lines
    for i= 1:size(lines,1)
        plot(lines(i,[1 3]),lines(i,[2 4]),'k--','LineWidth',1)
    end
    
    % Plot beacons
    for i= 1:size(beacs,1)
        plot(beacs{i,1},beacs{i,2},...
            'Color',cell2mat(beacs(i,3:5)),'Marker','o')
        text(beacs{i,1},beacs{i,2},['  ' beacs{i,6}])
    end
    
    % Plot virtual walls
    % Define virtual wall emitter constants
    halo_rad= 0.45;     % Radius of the halo around the emitter
    range_short= 2.13;  % Range of the wall on the 0-3' setting
    ang_short= 0.33;    % Angular range on the 0-3' setting
    range_med= 5.56;    % Range of the wall on the 4'-7' setting
    ang_med= 0.49;      % Angular range on the 4'-7' setting
    range_long= 8.08;   % Range of the wall on the 8'+ setting
    ang_long= 0.61;     % Angular range on the 8'+ setting
    
    % Get points to map virtual walls
    for i= 1:size(vwalls,1)
        x_vw= vwalls(i,1);
        y_vw= vwalls(i,2);
        th_vw= vwalls(i,3);
        if vwalls(i,4) == 1
            range_vw= range_short;
            ang_vw= ang_short;
        elseif vwalls(i,4) == 2
            range_vw= range_med;
            ang_vw= ang_med;
        else
            range_vw= range_long;
            ang_vw= ang_long;
        end
        x_1= x_vw+range_vw*cos(th_vw+ang_vw/2);
        y_1= y_vw+range_vw*sin(th_vw+ang_vw/2);
        x_2= x_vw+range_vw*cos(th_vw-ang_vw/2);
        y_2= y_vw+range_vw*sin(th_vw-ang_vw/2);
        
        % Plot halo around emitter and range triangle
        circ_numPts= 21;    % Estimate circle as circ_numPts-1 lines
        circ_ang=linspace(0,2*pi,circ_numPts);
        circ_rad=ones(1,circ_numPts)*halo_rad;
        [circ_x, circ_y]= pol2cart(circ_ang,circ_rad);
        circ_x= circ_x+x_vw;
        circ_y= circ_y+y_vw;
        plot(x_vw,y_vw,'g*')
        plot(circ_x,circ_y,'g:','LineWidth',1);
        plot([x_vw x_1 x_2 x_vw],[y_vw y_1 y_2 y_vw],'g:','LineWidth',1)
    end
end


% --- Executes on button press in push_config.
function push_config_Callback(hObject, eventdata, handles)
% hObject    handle to push_config (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Import configuration file
[filename, pathname, filter]= uigetfile('*.txt','Import Configuration File');
noise= struct;  % Initialize empty structure for noise information
if filename                         % Make sure cancel was not pressed
    fid= fopen([pathname filename]);% Get file handle for parsing
    
    % Get robot object for data entry
    obj= get(handles.text_title,'UserData');
    
    % Parse the file and extract relevant information
    while ~feof(fid)
        fline= fgetl(fid);      % Get line of file
        line= lower(fline);     % Convert to lowercase if necessary
        line= strtrim(line);    % Delete leading and trailing whitespace
        lineWords= {};          % To keep track of line entries
        while ~isempty(line) && ~strcmp(line(1),'%')% End of line or comment
            [word, line]= strtok(line);  % Get next entry
            line= strtrim(line);        % To be able to detect comments
            lineWords= [lineWords word];
        end
        
        % Get information from line
        sensors= {'wall' 'cliff' 'odometry' 'sonar' 'lidar' 'camera' 'imu'};
        if length(lineWords) == 2 && strcmp(lineWords{1},'com_delay')
            setComDelay(obj,str2double(lineWords{2}))
        elseif length(lineWords) == 3 && any(strcmp(lineWords{1},sensors))
            % Name the field in the noise structure after the sensor and
            % store the mean and standard deviation of the noise in it
            noise.(lineWords{1})= ...
                [str2double(lineWords{2}) str2double(lineWords{3})];
        elseif ~isempty(lineWords)
            warning('MATLAB:invalidInput',...
                'This line in config file %s is unrecognized:\n\t%s',...
                filename,fline)
        end
    end
    fclose(fid);
    
    % Set noise data in robot object
    setNoise(obj,noise)
end


% --- Executes on button press in push_origin.
function push_origin_Callback(hObject, eventdata, handles)
% hObject    handle to push_origin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Set origin data in robot object
obj= get(handles.text_title,'UserData');
[x, y]= ginput(2);   % Accept two mouse clicks
th= atan2(y(2)-y(1),x(2)-x(1)); % Direction is set by second mouse click
origin= [x(1), y(1), th];         % Start point is set by first mouse click
setMapStart(obj,origin);


% --- Executes on button press in chkbx_ir.
function chkbx_ir_Callback(hObject, eventdata, handles)
% hObject    handle to chkbx_ir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbx_ir

% Toggle visibility of sensor visualization with the checkbox
handles_sensors= get(handles.axes_map,'UserData');
if get(handles.chkbx_ir,'Value')
    set(handles_sensors(1:3),'Visible','on')
else
    set(handles_sensors(1:3),'Visible','off')
end
%}


% --- Executes on button press in chkbx_bump.
function chkbx_bump_Callback(hObject, eventdata, handles)
% hObject    handle to chkbx_bump (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkbx_bump

% Toggle visibility of sensor visualization with the checkbox
handles_sensors= get(handles.axes_map,'UserData');
if get(handles.chkbx_bump,'Value')
    set(handles_sensors(4:6),'Visible','on')
else
    set(handles_sensors(4:6),'Visible','off')
end

% --- Executes on button press in push_sensors.
function push_sensors_Callback(hObject, eventdata, handles)
% hObject    handle to push_sensors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get robot object to query sensors and output to command window
obj= get(handles.text_title,'UserData');
[rad, rI]= getConstants(obj);

% Bump sensors
bump= genBump(obj);
bumpR= bump(1);
bumpF= bump(2);
bumpL= bump(3);
fprintf('Bump Sensors:\n')
fprintf('\tRight: %.0f\n',bumpR)
fprintf('\tFront: %.0f\n',bumpF)
fprintf('\tLeft: %.0f\n\n',bumpL)

% Infrared wall sensors
distIR= genIR(obj);
fprintf('IR Sensors:\n')
fprintf('\tFront: %.3f m\n',distIR(1))
fprintf('\tLeft: %.3f m\n',distIR(2))
fprintf('\tRight: %.3f m\n\n',distIR(3))

% Odometry
distOdom= genOdomDist(obj);
angOdom= genOdomAng(obj);
fprintf('Odometry Data (since last call):\n')
fprintf('\tDistance: %.3f m\n',distOdom)
fprintf('\tAngle: %.3f m\n\n',angOdom)

% Overhead localization system
[x, y, th]= genOverhead(obj);
fprintf('Overhead Localization System output:\n')
fprintf('\tX-Coordinate: %.3f m\n',x)
fprintf('\tY-Coordinate: %.3f m\n',y)
fprintf('\tAngle relative to horizontal: %.3f rad\n\n',th)



% --- Executes on button press in push_auto_start.
function push_auto_start_Callback(hObject, eventdata, handles)
% hObject    handle to push_auto_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get robot object to pass on to control function
obj= get(handles.text_title,'UserData');

% Choose file to run as autonomous control function
[fileName, pathName, filter]= ...
    uigetfile('*.m','Load Autonomous Control File');
if fileName
    fileName= fileName(1:end-2);    % Take off '.m' extension
    if ~strcmp(cd,pathName) % If control program isn't in current directory
        addpath(pathName);   % Add the path to it to allow access
    end	% This will not save over to future sessions of MATLAB unless the user
    % has the setting to save the current search path upon exit
    
    % Set autonomous control to be enabled
    setAutoEnable(obj,true);
    
    % Start timer to determine time steps for data output and add first row
    startTimeElap(obj);
    updateOutput(obj);
    
    % Call odometry reading functions to zero readings
    genOdomAng(obj);
    genOdomDist(obj);
    
    % Catch error thrown when autonomous control is disabled and control
    % program calls a translator function, in order to halt control program
    %try
        % Run autonomous controller function
        feval(fileName,obj);
        
        % Run the Stop button callback to save data and re-enable manual
        % This will only run if Stop is not pushed during autonomous
        hObject_new= handles.push_auto_stop;
        push_auto_stop_Callback(hObject_new,[],handles);
%{ 
   catch me                    % Catch error and information
        if strcmp(me.identifier,'SIMULATOR:AutonomousDisabled')
            % EAB manualKeyboard(obj,[NaN NaN])   % Stop robot movement
            return                          % Allow GUI control again
        else                % Not error from control being disabled
            rethrow(me)     % Allow error to propagate to command window
        end
    end
    %}
end


% --- Executes on button press in push_auto_stop.
function push_auto_stop_Callback(hObject, eventdata, handles)
% hObject    handle to push_auto_stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get robot object
obj= get(handles.text_title,'UserData');

SetFwdVelAngVel(obj,0,0);

% Set autonomous control to be disabled
setAutoEnable(obj,false);

% Export and reset the output data
if get(handles.chkbx_auto_save,'Value')
    saveOutput(obj);
end
resetOutput(obj);

return


% --- Executes on button press in radio_centric.
function radio_centric_Callback(hObject, eventdata, handles)
% hObject    handle to radio_centric (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radio_centric
