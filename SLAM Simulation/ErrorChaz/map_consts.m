%% Map Constants
% Author: Connor Morales
% 
% Description:
%   This function is used to tell the control program the map configuration.  It
%   should always match the map used in simulation.
%   The i^th row of 'beacons' should correspond to the i^th element in
%   'landmarks'.  All beacons specified in the map should be redeclared in this
%   function.
%   The limits of the map should also be specified in 'lims'.
% 

function [beacons, landmarks, N, pose] = map_consts()

	% Beacon locations
	beacons = [
		 0.0  0.0 ;
		 0.0  1.0 ;
		-1.0  0.0 ;
		-1.0  1.0 ;
		 1.0  0.0 ;
		 1.0  1.0
	];
	
	% Beacon markers (same order as 'beacons' var)
	landmarks = (1:6)';
	
	% Number of landmarks
	if length(landmarks) ~= size(beacons,1)
		error('The number of beacons must be the same as the number of landmarks');
	end
	N  = length(landmarks);
	
	% Robot position
	pose = [
		 0.0 ;
		-1.0 ;
		 pi/2
	];
	
end
