%% Control Function  - Error Characterization - Feature Measurement
% Author: Connor Morales
% 

function [] = Control_Features(robotObj)

	% Number of samples
	N = 4096;

	% Set up robot pose and get landmark locations
	[beacons, landmarks, Nb, pose] = map_consts();
	setMapStart(robotObj, pose);
	
	
	% Calculate actual distance values
	rAct = zeros(Nb,1);
	aAct = zeros(Nb,1);
	for l = 1 : Nb
		% Get distances
		x = beacons(l,1) - pose(1);
		y = beacons(l,2) - pose(2);
		t = pose(3);
		% Convert to polar and save to vector
		rAct(l) = sqrt(x^2 + y^2);
		aAct(l) = atan2(y, x) - t;
	end
	
	
	% Initialize measurement vectors
	rad = zeros(N,Nb);
	ang = zeros(N,Nb);

	% Feature Measurements
	for n = 1 : N
		
		% Read beacon sensor
		[x, y, s] = ReadBeacon(robotObj);
		
		% We must see all landmarks
		if length(x) ~= Nb
			error('Could not see all beacons (%i), saw (%i)', Nb, length(x));
		end
		
		% Go through all landmarks and save values
		for l = 1 : Nb
			% Get index of landmark with this correspondence
			i = find(landmarks == s(l));
			% Convert to polar and save to vector
			rad(n,l) = sqrt(x(i)^2 + y(i)^2);
			ang(n,l) = atan2(y(i), x(i));
		end
		
	end
	
	
	
	% Analysis of readings
	rMean = mean(rad, 1);
	rStd  = std(rad, 0, 1);
	aMean = mean(ang, 1);
	aStd  = std(ang, 0, 1);
	
	
	% Display
	for l = 1 : Nb
		fprintf('BEACON %i (%i)\n', l, landmarks(l));
		fprintf(' RADIUS\n');
		fprintf('  Actual: %.4f\n', rAct(l));
		fprintf('  Mean--: %.4f\n', rMean(l));
		fprintf('  STD---: %.4f\n', rStd(l));
		fprintf(' ANGLE\n');
		fprintf('  Actual: %.4f\n', aAct(l));
		fprintf('  Mean--: %.4f\n', aMean(l));
		fprintf('  STD---: %.4f\n', aStd(l));
		fprintf('\n');
	end
	
	fprintf('\n');
	fprintf('Radius Mean STD: %.4f\n', mean(rStd));
	fprintf('Angle Mean STD-: %.4f\n', mean(aStd));
		
end













