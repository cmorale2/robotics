%% Control Function  - Error Characterization - Distance Measurement
% Author: Connor Morales
% 
% NOTE: This control function may take a LONG time depending on parameters.
%       The simulation time is N*Nc*(dt+0.1), so take this into account.
% 
% 

function [] = Control_Distance(robotObj)

	% Number of samples (keep relatively small so robot doesn't escape bounds)
	N = 16;
	
	% Number of unique controls 
	Nc = 16;

	% Get robot initial pose
	pose = [0,0,pi/2];
	setMapStart(robotObj, pose);
	
	
	% Maximum velocity
	vmax = 0.5;
	wmax = pi/2;
	
	% Time step
	dt = 0.1;
	
	
	
	
	% Location variance
	mpose = zeros(3,Nc);
	spose = zeros(3,Nc);
	for c = 1 : Nc
		
		% Pick a control action
		if rand() > 0.5
			v = vmax*rand();
			w = 0;
		else			
			if rand() > 0.5
				v = vmax * rand();
				wlim = min([v2w(v), wmax]);
				w = 2*wlim*rand() - wlim;
			else
				w = 2*wmax*rand() - wmax;
				vlim = w2v(w);
				v = vlim*rand();
			end
		end
		fprintf('Command (%.2f,%.2f):\n', v, w);
		
		
		% Reset pose
		pose = [0,0, 2*pi*rand()];
		setMapStart(robotObj, pose);
		
		
		% Do it several times
		fpose = zeros(3,N);
		for n = 1 : N
			
			% Check if we've stopped
			if ~robotObj.autoCheck
				return;
			end
			
			% Move robot
			SetFwdVelAngVel(robotObj, v, w);
			pause(dt);
			SetFwdVelAngVel(robotObj, 0, 0);
			
			% Pause to emulate computation
			pause(0.1);

			% Get robot position
			[xa,ya,ta] = OverheadLocalization(robotObj);
			ta = mod(ta+2*pi, 2*pi);
			
			% Get estimate position
			[xe,ye,te] = motion_update(pose, v,w,dt);
			te = mod(te+2*pi, 2*pi);
			
			% Save new pose
			pose = [xa;ya;ta];
			
			% Save
			fpose(1,n) = abs(xa-xe);
			fpose(2,n) = abs(ya-ye);
			fpose(3,n) = mod(abs(ta-te)+pi, 2*pi) - pi;
			
		end
		
		% Display mean and STD
		mpose(:,c) = mean(fpose, 2);
		spose(:,c) = std(fpose, 0, 2);
		fprintf('X %.2f (%.2f)\n', mpose(1,c), spose(1,c));
		fprintf('Y %.2f (%.2f)\n', mpose(2,c), spose(2,c));
		fprintf('T %.2f (%.2f)\n', mpose(3,c), spose(3,c));
		
	end
	
	% Display mean of STDs
	mm = mean(mpose, 2);
	sm = std(mpose, 0, 2);
	fprintf('Final:\n');
	fprintf('X: %.2f-> %.2f <-%.2f - (%.2f)\n', min(mpose(1,:)), mm(1), max(mpose(1,:)), sm(1));
	fprintf('Y: %.2f-> %.2f <-%.2f - (%.2f)\n', min(mpose(2,:)), mm(2), max(mpose(2,:)), sm(2));
	fprintf('T: %.2f-> %.2f <-%.2f - (%.2f)\n', min(mpose(3,:)), mm(3), max(mpose(3,:)), sm(3));
	
	
	
	
	
	
	
	
			
end




function w = v2w(v)
% Calculate the maximum allowable angular velocity from the linear velocity
%
% Input:
% v - Forward velocity of Create (m/s)
%
% Output:
% w - Angular velocity of Create (rad/s)
    % Robot constants
    maxWheelVel= 0.5;   % Max linear velocity of each drive wheel (m/s)
    robotRadius= 0.08;   % Radius of the robot (m)

    % Max velocity combinations obey rule v+wr <= v_max
    w= (maxWheelVel-v)/robotRadius;
end

function v = w2v(w)
    % Robot constants
    maxWheelVel= 0.5;   % Max linear velocity of each drive wheel (m/s)
    robotRadius= 0.08;   % Radius of the robot (m)

    % Max velocity combinations obey rule v+wr <= v_max
	v = maxWheelVel - abs(w)*robotRadius;
end







function [x,y,t] = motion_update(pose, v,w,dt)

	% Typesaving
	x = pose(1);
	y = pose(2);
	t = pose(3);

	% FKM
	if w == 0
		x = x + dt*v*cos(t);
		y = y + dt*v*sin(t);
		
	% ICR
	else
		vrat = v/w;
		x = x - vrat*sin(t) + vrat*sin(t + w*dt);
		y = y + vrat*cos(t) - vrat*cos(t + w*dt);
		t = t + w*dt;
	end

end



