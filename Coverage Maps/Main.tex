\documentclass[10pt,letterpaper]{article}

\usepackage[utf8]{inputenc}


\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{amsfonts}


\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{float}

\usepackage{cite}





\title{
	Coverage Maps\\
	Implementation Notes
}
\author{
	Connor Morales\\
	\texttt{c\_morales2@u.pacific.edu}
}
\date{\today}


\newcommand{\dd}{\,d}
\renewcommand{\vec}[1]{\mathbf{#1}}


%% Document
\begin{document}
\maketitle
\newpage


\section{Introduction}
These are implementation notes for coverage maps as used in MATLAB simulation.  Coverage maps are introduced by Stachniss \& Burgard \cite{coverage_maps}, and their paper should be read before these notes.  The implemented coverage maps are not exactly as described in the reference paper.  The differences are indicated in these notes, and explanations are provided.


\section{Overview}
Coverage maps are a probabilistic version of occupancy grids.  Whereas a cell in an occupancy grid can only have one of two values (occupied or empty), a cell in a coverage map contains a measure of probability as to whether that cell is occupied or not.  This is useful in cases such as Figure \ref{fig:map_grid}, where an obstacle does not occupy an entire grid square, but rather covers only part of it.

Specifically in our case, each cell is represented as a probability histogram (Figure \ref{fig:cell_hist}).  Each bin of the histogram represents a certain amount of coverage.  The value of each bin ranges from 0 to 1 and represents the probability that the cell has that amount of coverage.  Coverage values range from 0 to 1, with 0 meaning the cell is completely empty, and 1 meaning the cell is completely occupied.

An unexplored cell will have a probability histogram where all bins have the same value.  This means all coverage values are equally likely, which makese sense because there is no information about the cell available.  As more information is gathered about the cell, a single coverage value will have a higher probability than the others.  In order to determine a cell's actual coverage value, the coverage bin with the highest probability should be found.  If more than one bin have the highest probability, we take the mean of them to obtain the actual coverage value.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\centering
		\includegraphics[width=0.9\textwidth]{Images/Grid}
		\caption{Map grid with obstacles not occupying entire cells}
		\label{fig:map_grid}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.55\textwidth}
		\centering
		\includegraphics[width=\textwidth]{Images/Hist}
		\caption{Single grid cell coverage representation}
		\label{fig:cell_hist}
	\end{subfigure}
	\caption{Example map grid and cell histogram}
\end{figure}




\section{Cell Updates}
Each new distance measurement from a sensor produces its own probability histogram for a cell. Equation \eqref{eq:prob_hist} tells us the probability that a given cell $c_l$ has coverage value $x$ given a distance measurement $d_m$.  To obtain the full histogram, simply evaluate \eqref{eq:prob_hist} for all bins.

\begin{equation} \label{eq:prob_hist}
p(c_l = x ~|~ d_m) = \mathcal{N}\left( \mu(d_c, d_m), \sigma(d_c, d_m), x \right)
\end{equation}
\noindent where: \\
\indent $p$ is probability \\
\indent $c_l$ is the grid cell in question \\
\indent $x$ is a coverage value \\
\indent $d_c$ is the distance from the sensor to the cell $c_l$ \\
\indent $d_m$ is the sensor's reported distance measurement \\
\indent $\mathcal{N}$ is a Gaussian function \\
\indent $\mu$ is the Gaussian mean, given by \eqref{eq:prob_mean} \\
\indent $\sigma$ is the Gaussian standard deviation, given by \eqref{eq:prob_std} \\

The reference paper also includes a ``uniform distribution''.  After various experiments and several iterations, it was removed from this implementation.  All attempts to include it resulted in nonsensical results.


\subsection{Mean}
The mean of this Gaussian is a piecewise given by \eqref{eq:prob_mean}.  Let us assume our grid is uniform where each cell is a square with side length $r$.  When deciding what the mean value of the Gaussian should be, we must consider the range of one cell width around the sensor measurement distance.

\begin{equation} \label{eq:prob_mean}
\mu(d_c,d_m) = \begin{cases}
0, & d_c < d_m-\frac{r}{2} \\
1, & d_c > d_m+\frac{r}{2} \\
\frac{\frac{r}{2} + (d_c - d_m)}{r}, & d_m-\frac{r}{2} \leq d_c \leq d_m+\frac{r}{2}
\end{cases}
\end{equation}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Images/Mean}
	\caption{Mean given by \eqref{eq:prob_mean} with a measurement distance $d_m=5$.  The blue section represents case 1, the red section case 2, and the black section case 3.}
	\label{fig:prob_mean}
\end{figure}

Let us examine a scenario for each case in \eqref{eq:prob_mean}.  The robot is the blue object, the obstacle being measured is the green object, and the cell(s) being considered is(are) highlighted in red.

\begin{enumerate}
	\item If the cell being considered is between the robot (sensor) and the sensor distance measurement, the mean should be zero, because that cell is empty.
	\begin{center}
		\includegraphics[scale=1]{Images/Dist_Before}
	\end{center}

	\item If the cell being considered is past the sensor distance measurement, the mean should be one, because that cell is occupied by the obstacle.
	\begin{center}
		\includegraphics[scale=1]{Images/Dist_After}
	\end{center}

	\item If the cell being considered is within one cell's width of the sensor distance measurement, the mean is a linear function starting from $0$ close to the robot and ending at $1$ farther from the robot.  Both cells highlighted below are within this range.
	\begin{center}
		\includegraphics[scale=1]{Images/Dist_Mid}
	\end{center}

\end{enumerate}

There is a fourth case of when the cell being considered is past the obstacle.  In an unknown environment, there is no way to know the thickness of an obstacle.  In the reference paper, there is a brief mention of a $20cm$ distance past a measurement that is considered.  In our implementation, we specify a parameter $d_{wall}$ that defines the limit of cells to be updated.  Only cells that lay within $d_m+d_{wall}$ of the robot will be updated for a given measurement $d_m$.

The reference paper never make it clear, but based on the figures, it appears as though coverage maps as defined are only meant to be used to map the bounds of an obstacle-free room.  This is why our parameter is denoted $wall$ - our implementation assumes that only walls of a room will be measured.




\subsection{Standard Deviation}
The standard deviation equation \eqref{eq:prob_std} takes into account two factors: (1) the inherent error associated with the distance sensors used, and (2) the uncertainty of a cell's coverage when it is close to a distance measurement.

As the distance a sensor measures increases, the sensor will inherently become less accurate.  This implementation's standard deviation equation is based on a linear fit of a sensor's error.  The values of this fit can be empirically found by characterizing the sensor in question.

This linear fit is altered for the case where the cell being considered is within one cell's width of the sensor distance measurement (the same cells with a linear mean equation).  In this case, the standard deviation is a constant value that is larger than all others in the equation.

\begin{equation} \label{eq:prob_std}
\sigma(d_c,d_m) = \begin{cases}
\sigma_{large}, & d_m-\frac{r}{2} \leq d_c \leq d_m+\frac{r}{2} \\
(\sigma_{max}-\sigma_{min})\frac{d_c}{d_{max}} + \sigma_{min}, & else
\end{cases}
\end{equation}

\noindent where: \\
\indent $d_{max}$ is the maximum valid distance the sonar sensor can reliable measure \\
\indent $\sigma_{min}$ is the standard deviation of sensor measures close to distance $0$ \\
\indent $\sigma_{max}$ is the standard deviation of sensor measures close to $d_{max}$ \\
\indent $\sigma_{large}$ is a value larger than all other standard deviations \\

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Images/STD}
	\caption{Standard Deviation given by \eqref{eq:prob_std} with a measurement distance $d_m=5$.  The blue sections correspond to the sensor error, and the red section corresponds to the special case.}
	\label{fig:prob_std}
\end{figure}

Note that we now have defined a maximum sensor distance $d_{max}$.  In the case that we obtain a sensor measurement $d_m \geq d_{max}$, we should not perform the cell update process as normal.  If we did, it would result in an obstacle being recorded that is at best inaccurately located and at worse nonexistent.  Instead, we should perform the update only for cells where $d_c < d_{max}$, setting their mean to $0$, and their standard deviation to the correct value in the linear fit equation. 


\subsection{Combining Measurements}
All cells of the map are initialized as a uniform distribution where the sum over all bins results in a probability of $1$.  When a new measurement is taken, the probability histogram is calculated using \eqref{eq:prob_hist}.  This histogram should be normalized such that it sums to a probability of $1$.  The new histogram should then be multiplied elementwise by the current cell probability.  Finally, the resulting histogram should again be normalized so that it sums to a probability of $1$.  This normalization process is not mentioned in the reference paper, but it is required to ensure that each cell's probabilities always sum to $1$.  If this did not hold true, then probability laws would not either.



\section{Uncertainty}
In addition to knowing how covered a cell most likely is, we must also have a way to measure how certain we are of that coverage value.  Our uncertainty measure is entropy of the probability histogram.  Assuming all bins of the histogram sum to $1$, we can calculate entropy using \eqref{eq:entropy}.

\begin{equation} \label{eq:entropy}
H(h) = - \sum_{i=1}^{n} h_i\log(h_i)
\end{equation}

\noindent where: \\
\indent $H$ is entropy \\
\indent $h_i$ is the $i^{th}$ bin of the histogram \\
\indent $\log(x)$ is the natural logarithm of $x$ \\

Entropy will be maximal in the case of a uniform distribution.  This maximal amount will depend on the number of bins in the histogram $H$.  Entropy will be minimal in the case where a single bin has probability value $1$, and the rest have value $0$, which would occur when the occupancy of a cell is completely certain.





\section{Example}
Consider the case where we have a maximum sensor distance of $150$, a wall thickness of $20$, a cell side length of $5$, and a measured distance of $95$.  We can generate probability histograms for every cell starting from the sensor location and ending with the maximum ditance.  We can also calculate the uncertainty of each cell's probability histogram.  Both of these are given in Figure \ref{fig:ex_dist1}.

For the probability map, each row represents the probability histogram of a single cell.  Thus, each row sums to $1$.  The coverage value of each cell (row) is marked with a red dot, and the distance measured is marked with a horizontal red line.  Note that the cell is referenced at its center point.  The sensor is located at the center of the first cell at distance $0$, which means the distance measure of $95$ is in the center of the $19^{th}$ cell from the sensor.

For the uncertainty plot, each cell is marked as a blue dot.  The distance measured is marked with a vertical red line.  The title contains the highest entropy value of all updated cells (which does not include any cells past $d_m + d_{wall}$).

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Images/Ex_Dist1}
	\caption{Example Probability Map, Distance $95$}
	\label{fig:ex_dist1}
\end{figure}

On the probability map, note first that all cells past the measured distance plus the wall thickness ($d_m+d_{wall}$) are not updated.  We do not know anything about them, so all coverage values are equally probable, and thus their coverage value is $0.5$.  Note that the uncertainty of these cells is also maximal.  Even though the cell located directly at the measured distance also has a coverage value of $0.5$, its uncertainty is lower.

The effects of the mean function should be obvious.  All cells between the sensor and measurement distance have coverage $0$, and all cells past the measurement distance but before the wall thickness have coverage $1$.  The measured distance is directly in the center of the $19^{th}$ cell, which means only it has a special case mean function, and it also evaluates directly to coverage $0.5$.

The effects of the standard deviation function are less obvious, but still noticeable.  For the non-special case cells, we can see that as the cell distance increases, the probability of their most likely coverage value decreases (less yellow), and the probability of neighboring bins increases (lighter blue and green).  This can also be seen in the uncertainty plot by a steadily increasing linear trend.

The standard devation of the special case cell is much higher, shown by all its bins being blue rather than yellow or even green.  The spike in the uncertainty plot also shows this.


Examine now Figure \ref{fig:ex_dist2}, which is the same case except the measured distance is $105$ instead of $95$.  We can see all the same trends as with the previous distance measurement, only shifted so that the special case cell is the $21^{st}$ instead of $19^{th}$.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Images/Ex_Dist2}
	\caption{Example Probability Map, Distance $105$}
	\label{fig:ex_dist2}
\end{figure}




Image that the two distance measurements given by Figures \ref{fig:ex_dist1} and \ref{fig:ex_dist2} come from the same stationary sensor measuring the same object.  In this secnario, our sensor has an error range of $\pm5$, and that the actual obstacle is located at distance $100$.  When we combine the two measurements, we should obtain a better idea of what the map actually looks like.  That is, we should see the obstacle on the coverage map at the correct distance, and the uncertainty of all our measured cells should decrease from either of the two individual measurments.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Images/Ex_DistComb}
	\caption{Example Probability Map, Combination $95$ and $105$}
	\label{fig:ex_distcomb}
\end{figure}

Rather unsurprisingly, Figure \ref{fig:ex_distcomb} shows our expectations to be true.  We see that the cell at distance $100$ has a coverage value of $0.5$, which is to be expected if an obstacle is located there.  We also see that the cells at distance $95$ and $105$ are approaching their actual values of $0$ and $1$, respectively.  The uncertainty plot shows that the entropy of all measured cells has decreased, meaning we are more sure of our map.

There are two exceptions to this decreased entropy trend.  The first one is for cells updated by measurement $105$ but not measurement $95$.  These cells are considered part of the wall by measurement $105$ but were considered to be past the wall by measurement $95$.  These cells that were only updated for the further measurement did not change their uncertainty when the two measurements were combined because no new information was available.

The second exception is for the cell at distance $100$ - directly between our two measurement values.  Our two measurements had this cell's coverage (and mean) at complete opposite sides of the scale.  Therefore, when the measurements are combined, a lot of uncertainty is expected.  Even though the uncertainty of this cell increased compared to its previous values, it is still less than the maximum uncertainty of both individual measurements.  As more measurements are taken and included in the map, the uncertainty will trend to $0$.






\paragraph{}
We just examined the case of where an object bisects the cell.  Let us now examine the case where the obstacle lines up with a cell edge.  Figure \ref{fig:ex_bw_single} shows the same situation as before with a distance of $102.5$.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{Images/Ex_BW_Single}
	\caption{Example Probability Map, Distance $102.5$}
	\label{fig:ex_bw_single}
\end{figure}

Notice that the means are in the correct place.  The standard deviation is large for the cells on either side of the measurement line, as our equation states.  This can be seen in the colors on the probability map and the uncertainty plot.  Note that there is less uncertainty in this situation than the previous one.










%% Bibliography
\newpage
% \renewcommand{\bibname}{\section{References}}
\bibliographystyle{IEEEtran}
\bibliography{sources}


\end{document}

