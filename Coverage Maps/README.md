# Robotics - Coverage Maps
## Connor Morales

***
This directory contains notes on coverage maps for robotics based on Stachniss & Burgard's paper "Exploring unknown environments with mobile robots using coverage maps".

***

A makefile is provided to compile the LaTeX.
```
make final
```

The MATLAB implementation can be found in the MATLAB directory.
