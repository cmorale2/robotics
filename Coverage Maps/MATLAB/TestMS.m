%% Mean and Standard Deviation Testing
% Author: Connor Morales

%% Cleanup
close all
clear
clc


%% Parameters

% Maximum distance
dmax = 10;

% Grid spacing
r = 1;

% Measured distance
dm = 5;

% Standard deviation limits
slims = [0.1, 0.3];
smax = 0.5;


%% Stuff

% Create grid
dc = linspace(0,dmax,101)';

% Calculate mean and standard deviation
[m,s] = meanstd(dc,dm,dmax,r,slims,smax);

% Find indices for ranges
i1 = (dc < dm-r/2);
i2 = (dm-r/2 <= dc & dc <= dm+r/2);
i3 = (dc > dm+r/2);

% Plot
figure('Name','Mean', 'Position', [100,100, 600,300]);
hold on
plot(dc(i1), m(i1), 'b-', 'LineWidth',2);
plot(dc(i2), m(i2), 'k-', 'LineWidth',2);
plot(dc(i3), m(i3), 'r-', 'LineWidth',2);
ylim([-0.1,1.1]);
xlabel('Cell Distance');
ylabel('Mean');
grid on;
title(sprintf('Mean, Distance %i',dm));

figure('Name','Standard Deviation', 'Position',[100,100,600,300]);
hold on
plot(dc(i1), s(i1), 'b-', 'LineWidth',2);
plot(dc(i2), s(i2), 'r-', 'LineWidth',2);
plot(dc(i3), s(i3), 'b-', 'LineWidth',2);
ylim([0,smax+0.1]);
xlabel('Cell Distance');
ylabel('Standard Deviation');
grid on;
title(sprintf('Standard Deviation, Distance %i',dm));

