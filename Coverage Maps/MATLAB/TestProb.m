%% Probability Coverage Testing
% Author: Connor Morales


%% Cleanup
close all
clear
clc


%% Parameters

% Plot flags
plotSingle = true;
plotCum = false;
plotFinal = true;

% Bin width
bwidth = 0.05;

% maximum range of sonar sensors
dmax = 150;

% wall width
dwall = 20;

% Cell width
r = 5;

% Measurement values
dm = [102.5 102.5];

% Standard deviation values
slims = [0.1, 0.2];
smax = 0.3;


%% Stuff

% Bins
b = (0:bwidth:1)';
Nb = length(b);

% Uncertainty max (round up to whole # for plotting)
Um = ceil(entropy(ones(length(b),1)));

% Number of measurements
N = numel(dm);

% Cell distances
dc = (0 : r : dmax);
Nd = length(dc);

% Indices of note
ic = zeros(N,1); % last element of dc that is updated

% Initialize cell arrays
p = cell(N,1); % Probabilities
u = cell(N,1); % Means
s = cell(N,1); % Standard Deviations
ent = cell(N,1); % Entropy (uncertainty)


% Go through all measurement values and compute probability maps
for n = 1 : N
	
	% Find index of dc that is closest to current measured dist + wall dist
	[~,ic(n)] = min(abs(dc-dm(n)-dwall));
	ic(n) = ic(n) - 1;
	
	% Calculate mean and standard deviation
	[u{n},s{n}] = meanstd(dc, dm(n), dmax, r, slims, smax);
	
	% Initialize entropy at each distance
	ent{n} = zeros(Nd, 1);
	
	% Initialize probability map
	p{n} = zeros(Nd, Nb);
	
	% Populate probability map
	for m = 1 : Nd
		if dc(m) < dm(n)+dwall
			p{n}(m,:) = gaussian(u{n}(m), s{n}(m), b);
		else
			p{n}(m,:) = ones(1,Nb) / Nb;
		end
		ent{n}(m) = entropy(p{n}(m,:));
	end
	
	% Produce figure
	if plotSingle
		figure('Name', sprintf('Individual P # %i',n), 'Position',[100,100,1000,500]);

		% Probability histogram color plot
		subplot(1,2,1);
		pcolor(b,dc,p{n});
		colormap parula
		colorbar
		hold on
		% Measurement distance horizontal line
		plot([0,1],dm(n)*[1,1]+r/2, 'r-'); % Offset by r/2 b/c the way pcolor grid works
		% Maximal coverage value dot @ each cell
		for m = 1 : Nd
			M = max(p{n}(m,:));
			i = find(p{n}(m,:) == M);
			ii = round(mean(i));
			plot(b(ii), dc(m)+r/2, 'r.', 'MarkerSize',10); % Offset by r/2 b/c the way pcolor grid works
		end
		xlim([0,1]);
		ylim([0,max(dc)]);
		xlabel('Coverage');
		ylabel('Cell Distance');
		title('Probability Map');

		% Uncertainty plot
		subplot(1,2,2);
		plot(dc,ent{n},'b.:', 'MarkerSize',10);
		hold on
		% Distance measurement lines
		ptemp = plot(dm(n)*[1,1], [0,Um], 'r-');
		ptemp.Color(4) = 0.5;
		grid on
		grid minor
		xlim([0,max(dc)]);
		ylim([0,Um]);
		xlabel('Cell Distance');
		ylabel('Entropy');
		title(sprintf('Uncertainty (%.2f)',max(ent{n}(1:ic(n)))));

		suptitle(sprintf('Distance %i (%.1f)',n,dm(n)));
	end
	
end



% Initialize cell arrays
pc = cell(N+1, 1); % Cumulative probability
pc{1} = ones(size(p{1}))/Nd; % Initial probability is uniform
entc = cell(N,1); % Uncertainty
icc = 1; % Index of maximum measured distance

% Go through distance measurements 1:end, calculate cumulative probability maps
for n = 1 : N
	
	% Find index for maximum distance measure up to n
	icc = max([icc, ic(n)]);
	
	% Compute cumulative probability
	pc{n+1} = pc{n} .* p{n};
	
	% Reset cumulative probability to have sum 1
	for m = 1 : Nd
		pc{n+1}(m,:) = pc{n+1}(m,:) / sum(pc{n+1}(m,:));
	end
	
	% Compute uncertainty
	entc{n} = zeros(Nd,1);
	for m = 1 : Nd
		entc{n}(m) = entropy(pc{n+1}(m,:));
	end
	
	% Produce figure
	if plotCum || (plotFinal && n == N)
		figure('Name',sprintf('CumProb 0-%i',n), 'Position',[100,100,1000,500]);

		% Probability histogram color plot
		subplot(1,2,1)
		pcolor(b,dc,pc{n+1});
		colormap parula
		colorbar
		hold on
		% Measurement distance horizontal line(s)
		for i = 1 : n
			plot([0,1],dm(i)*[1,1]+r/2, 'r-'); % Offset by r/2 b/c the way pcolor grid works
		end
		% Maximal coverage value dot @ each cell
		for m = 1 : Nd
			M = max(pc{n+1}(m,:));
			i = find(pc{n+1}(m,:) == M);
			ii = round(mean(i));
			plot(b(ii), dc(m)+r/2, 'r.', 'MarkerSize',10); % Offset by r/2 b/c the way pcolor grid works
		end
		xlim([0,1]);
		ylim([0,max(dc)]);
		xlabel('Coverage');
		ylabel('Cell Distance');
		title('Cumulative Probability Map');

		% Uncertainty plot
		subplot(1,2,2)
		plot(dc,entc{n},'b.:', 'MarkerSize',10);
		hold on
		% Distance measurement lines
		for i = 1 : n
			ptemp = plot(dm(i)*[1,1], [0,Um], 'r-');
			ptemp.Color(4) = 0.5;
		end
		grid on
		grid minor
		xlim([0,max(dc)]);
		ylim([0,Um]);
		xlabel('Cell Distance');
		ylabel('Entropy');
		title(sprintf('Uncertainty (%.2f)',max(entc{n}(1:icc))));

		suptitle(sprintf('Distances 0-%i',n));
	end
	
end

