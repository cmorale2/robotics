%% Probability Histogram
% Author: Connor Morales

%% Cleanup
close all
clear
clc


%% Stuff

% Initialize bins
b = (0:0.05:1)';

% Compute Gaussian centered at 0.2, std 0.3
G = gaussian(0.22, 0.05, b);

% Normalize gaussian to sum of 1
G = G / sum(G);

% Display histogram
figure('Name','Histogram');
bar(b,G,'b');
xlim([0,1]);
xlabel('Coverage');
ylabel('Probability');
title('Coverage Probability Histogram');
