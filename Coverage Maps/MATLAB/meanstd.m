%% Mean and Standard Deviation for Gaussian
% Author: Connor Morales
% 
% Description:
%   This function generates a mean and standard deviation for the Gaussian used
%   by Coverage Maps.
% 
% Input:
%   - Cell distances (vector)
%   - Measurement distance
%   - Maximum measurement distance possible
%   - Cell side length
%   - Standard deviation limits (min, max) (2-length vector)
%   - Standard deviation for 1-cell width area
% 
% Output:
%   - Mean (vector, size of input 1)
%   - Standard Deviation (vector, size of input 1)
% 

function [mu,s] = meanstd(dc,dm,dmax,r,slims,smax)

	% Distance difference
	d = dc - dm;
	
	% Mean
	mu = zeros(size(d));
	% u(d < -r/2) = 0;
	mu(d > r/2) = 1;
	i = ( abs(d) <= r/2 );
	mu(i) = (r/2 + d(i)) / r;
	
	% Standard deviation
	s = (slims(2)-slims(1)) * (dc)/(dmax) + slims(1);
	s(i) = smax;

end
