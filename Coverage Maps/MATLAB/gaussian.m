%% Gaussian Function
% Author: Connor Morales
% 
% Description:
%   This function computes a Gaussian given the mean, standard deviation, and a
%   vector of values.  It will be scaled such that the sum over all input values
%   is unity.
% 
% Input:
%   - Mean
%   - Standard Deviation
%   - Values (vector)
% 
% Output:
%   - Gaussian (vector, size of input 3)
% 

function N = gaussian(mu,std,x)
	% Calculate gaussian function
	N = exp(-(x-mu).^2 / (2*std^2)) / (std * sqrt(2*pi));
	% Set so sum is 1
	N = N / sum(N);
end

