%% Uncertainty Measure
% Author: Connor Morales
% 
% Description:
%   This function computes the entropy of a vector of probabilities.  If the
%   vector does not sum to 1, it will be scaled so that it does.
%   It will be maximal given a uniform distribution and minimal with a single
%   probability of 1 and the rest 0.
% 
% Input:
%   - Probabilities (vector)
% 
% Output:
%   - Entropy (vector, size of input)
% 

function H = entropy(p)
	% Probabilities should sum to 1
	p = p / sum(p);
	% Entropy calculation
	H = -sum(p .* log(p));
end
