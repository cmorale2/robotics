\documentclass[10pt,letterpaper]{article}

\usepackage[utf8]{inputenc}


\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{amsfonts}

\setcounter{MaxMatrixCols}{20}


\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{float}

\usepackage{cite}





\title{
	SLAM Documentation
}
\author{
	Connor Morales\\
	\texttt{c\_morales2@u.pacific.edu}
}
\date{\today}


\newcommand{\dd}{\,d}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\var}[1]{\mathcal{#1}}


%% Document
\begin{document}
\maketitle
\newpage





\section{Motion Models}
Motion models describe how a robot moves in response to control actions.  They take control actions (movement commands) and convert them to a global position.  All motion models are specific to a robot's configuration.  Our models focus on differential drive robots.

% TODO: add image that shows labeled directions

We describe a robot's pose (position) at time $t$ using a vector that contains the robot's global coordinates and rotation relative to the global $x$-axis.
$$ \var{X}_t \doteq \begin{bmatrix}x_t \\ y_t \\\theta_t\end{bmatrix} $$

A motion model acts on the previous pose (time $t-1$) and the control action $u$ to obtain the current pose.
$$ \var{X}_t = f(\var{X}_{t-1}, u_t) $$

A control action in our case is a set of a linear velocity $v$, rotational velocity $w$, and a time step $\Delta t$.
$$ u_t \doteq \{v_t,w_t,\Delta t\} $$



\subsection{Forward Kinematic Model}
The Forward Kinematic Model (FKM) is used when the control action contains no rotational velocity.  The robot is simply moving straight, so we can use a linear distance equation to obtain its new relative pose.
$$ \var{X}_{robot} = \begin{bmatrix}v_t \Delta t\\0\\0\end{bmatrix} $$

% TODO: add image showing scenario

This robot pose needs to be converted to global coordinates, which we can do using a rotational transform matrix.  If we rotate the relative robot pose by the global robot angle, we can simply add the old pose to obtain the new.
$$ \var{X}_t = \var{X}_{t-1} +
\begin{bmatrix}
\cos(\theta_{t-1}) & -\sin(\theta_{t-1}) & 0 \\
\sin(\theta_{t-1}) &  \cos(\theta_{t-1}) & 0 \\
0                  &  0                  & 1
\end{bmatrix}
\var{X}_{robot} $$

This can be multiplied out to obtain a final, simplified FKM update equation.
$$ \var{X}_t = \var{X}_{t-1} + \begin{bmatrix}v_t\Delta t\cos(\theta_{t-1}) \\ v_t\Delta t\sin(\theta_{t-1})\\0\end{bmatrix} $$



\subsection{Instantaneous Center of Rotation}
The Instantaneous Center of Rotation (ICR) model is used when the control action involves a rotational velocity.  For a given linear velocity and rotational velocity, the robot can be though of as traveling an arc of a circle with radius $R$.  We assume the time step is small enough that the robot is traveling the same circle arc for its entirety.

% TODO: add image to illustrate

The equation for the relative new angle should obviously just be the rotational velocity multiplied by the time step.  The new global angle is just the old one added to the new relative one.
$$ \theta_t = \theta_{t-1} + w_t\Delta t $$

We can calculate the global position of the center of the circle using simple geometry and trigonometry.
$$ \var{X}_{ICR} = \begin{bmatrix}x_c\\y_c\end{bmatrix} = \begin{bmatrix}x_{t-1}\\y_{t-1}\end{bmatrix} + \begin{bmatrix}-R\sin(\theta_{t-1})\\R\cos(\theta_{t-1})\end{bmatrix} $$

We now change only the angle of the robot, and we must now calculate the change in robot position.  Since the ICR should not change for a given time step, if we calculate how much this change in angle shifts the ICR, we will know how much the robot's position must change.
$$ \var{X}_{shift} = \var{X}_{ICR} - \begin{bmatrix}-R\sin(\theta_{t})\\R\cos(\theta_{t})\end{bmatrix} $$

We can expand our pose equation to its common form.  We also add the new angle we calculated previously.
$$ \var{X}_t = \var{X}_{t-1} +
\begin{bmatrix}
-R\sin(\theta_{t-1}) + R\sin(\theta_{t-1}+w\Delta t) \\
 R\cos(\theta_{t-1}) - R\cos(\theta_{t-1}+w\Delta t) \\
 w\Delta t
\end{bmatrix}
,\quad R = \frac{v_t}{w_t}
$$












\section{Extended Kalman Filter SLAM}
Extended Kalman Filter (EKF) SLAM is the earliest form of SLAM.  It is ``extended'' because it uses nonlinear models, and a Kalman filter because it represents the robot and map using Gaussians.  We will be considering an online SLAM algorithm.  There are three assumptions (or approximations) made for this SLAM algorithm.
\begin{enumerate}
	\item Robot motion and sensor error are Gaussian in nature.
	\item We only process positive sightings of landmarks.
	\item Maps are feature-based.
\end{enumerate}


Our goal is to solve the probabilistic equation for the current pose.
$$ p(\var{X}_t ~|~ z_{1:t-1}, u_{1:t}) $$
\noindent where: \\
\indent $\var{X}_t$ is current pose at time $t$ \\
\indent $z_{1:t-1}$ is the set of all previous landmark measurements \\
\indent $u_{1:t}$ is the set of all control actions \\

Since we are using a Kalman filter, all of these variables are Gaussian in nature.  Thus, we only need to track mean and covariance.



\subsection{Variables}
Our robot's pose is given as a vector of global coordinates.  Reproducing our previous definition here, we have:
$$ \var{X}_t = \begin{bmatrix}x_t\\y_t\\\theta_t\end{bmatrix} $$

Let $N$ be the number of landmarks in the map.  Our map is then given as a set of pose vectors representing each landmark.  As opposed to our robot's pose, we have a variable $s$ that represents a unique identifier for each landmark instead of an angle.
$$ m_t = \left\{ \begin{bmatrix}x_n\\y_n\\s_n\end{bmatrix} \right\}_{n=1}^{N} $$

We can now define a combined state vector that holds information of all the poses we care about.  It is called the \textit{true} state vector.  The goal of our algorithm is to get as close as possible to this true state.
$$ \var{Y}_t = \begin{bmatrix}\var{X}_t\\m_t\end{bmatrix} $$

Our \textit{estimate} state vector is a mean vector.  It represents the most likely location of all variables.  Note that a ``hat'' denotes that the variable is an estimate of the true value.
$$ \mu_t = \begin{bmatrix}\widehat{\var{X}}_t\\\widehat{m}\end{bmatrix} $$

We will be using full matrix notation from here on out by expanding our set of map pose vectors into a single vector.  It will have length $3N+3$ because the robot pose has $3$ elements, and each of the $N$ landmark poses have $3$ elements.
$$ \mu_t = \begin{bmatrix}x_t & y_t & \theta_t & r_{n,t} & \phi_{n,t} & s_{n,t} & \dots \end{bmatrix}^T $$

Our covariance will be represented as a square matrix of size $3N+3$.  The top left $3\times3$ section of the matrix is the covariance of the robot pose with itself.  The bottom right $3N\times3N$ section of the matrix is the covariance of the landmarks with themselves.  The other sections of the matrix are the covariance of the robot with the landmarks and vice versa.
$$
\Sigma_t = 
\begin{bmatrix}
% Top left
\begin{pmatrix}
robot-robot \\
3\times3
\end{pmatrix}
&
% Top Right
\begin{pmatrix}
robot-landmark \\
3\times N
\end{pmatrix}
\\
% Bottom Left
\begin{pmatrix}
landmark-robot \\
N\times3
\end{pmatrix}
&
% Bottom Right
\begin{pmatrix}
landmark-landmark \\
N\times N
\end{pmatrix}
\end{bmatrix}
$$


\subsection{Initialization}
At initial time $t=0$, the robot is at position $(0,0)$ by definition.  We declare its initial angle to be $0$ by convention.  We also state that all landmarks are at position $0$, but mark this as uncertain in the covariance matrix.
$$ \mu_t = \begin{bmatrix}0\\\vdots\\0\end{bmatrix} $$

Because the initial robot state is set by definition, it is absolutely certain.  Thus, the intial robot pose covariance is $0$, as is the covariance of the robot pose and landmark poses.  We have absolutely no idea where the landmarks are, so their variance is infinite.  Their covariance, however, is zero.
$$ \Sigma_t = 
\begin{bmatrix}
0 & 0 & 0 & 0 & \dots & 0 \\
0 & 0 & 0 & 0 & \dots & 0 \\
0 & 0 & 0 & 0 & \dots & 0 \\
0 & 0 & 0 & \infty & & 0 \\
\vdots & \vdots & \vdots & & \ddots  & \\
0 & 0 & 0 & 0 & & \infty \\
\end{bmatrix}
$$



\subsection{Movement Update}
Our true pose after movement is the previous pose adjusted by the motion model with appropriate error added.
$$ \var{Y}_t = f(\var{Y}_{t-1},u_t) + \var{N}(0,R_x) $$

\noindent where:\\
\indent $\var{N}$ is a Gaussian\\
\indent $R_x$ is covariance of movement measurement noise\\

Setting the noise aside for now, the pose update is nonlinear, which means we cannot use a Gaussian.  Instead, we must use a Taylor Series approximation.  This, of course, adds more error.
$$ g(\var{Y}_{t-1},u_t) = \var{Y}_{t-1} + f(\var{Y}_{t-1},u_t) $$
$$ g(\var{Y}_{t-1},u_t) \approx g(\mu_{t-1},u_t) + g'(\mu_{t-1},u_t)\left[\var{Y}_{t-1}-\mu_{t-1}\right] $$

Our mean update is completely based on our motion model because the noise has a mean of $0$.  The bar notation signifies that we have not yet considered landmark measurements in our update.
$$ \overline{\mu}_t = \mu_{t-1} + \begin{bmatrix}f(x_{t-1},u_t)\\f(y_{t-1},u_t)\\f(\theta_{t-1},u_t)\\0\\\vdots\end{bmatrix} $$

Note that we do not update the pose of the landmarks.  This is because the landmarks have not moved, only the robot.

\paragraph{Aside: Helper Matrix}
If, in our implementation, we have the motion update $f$ separated as a single $3$-length vector, we can utilize a $3\times(3N+3)$ ``helper matrix'' to update our pose.
$$ F_x = \begin{bmatrix}
1 & 0 & 0 & 0 & \dots & 0 \\
0 & 1 & 0 & 0 & \dots & 0 \\
0 & 0 & 1 & 0 & \dots & 0 \\
\end{bmatrix} $$

$$ \overline{\mu}_t = \mu_{t-1} + Fx^T\begin{bmatrix}f(x_{t-1},u_t)\\f(y_{t-1},u_t)\\f(\theta_{t-1},u_t)\end{bmatrix} $$

\paragraph{}
\paragraph{}
Our covariance matrix needs to be updated due to the error in both the movement itself and the measurement of that movement.  As was the case with the mean, we are not updating anything involving the landmarks because they do not move.

Let us examine the derivative term of our Taylor Approximation.
$$ g' = \frac{\partial g}{\partial \mu_{t-1}} = \frac{\partial}{\partial \mu_{t-1}} \mu_{t-1} + \frac{\partial}{\partial \mu_{t-1}} f $$

We can construct a Jacobian from this equation that can be used to update our robot's pose.
$$
G_t
=
\begin{bmatrix}
\frac{\partial \mu_{t,x}}{\partial \mu_{t-1,x}} & \frac{\partial \mu_{t,x}}{\partial \mu_{t-1,y}} & \frac{\partial \mu_{t,x}}{\partial \mu_{t-1,\theta}} \\
\frac{\partial \mu_{t,y}}{\partial \mu_{t-1,x}} & \frac{\partial \mu_{t,y}}{\partial \mu_{t-1,y}} & \frac{\partial \mu_{t,y}}{\partial \mu_{t-1,\theta}} \\
\frac{\partial \mu_{t,\theta}}{\partial \mu_{t-1,x}} & \frac{\partial \mu_{t,\theta}}{\partial \mu_{t-1,y}} & \frac{\partial \mu_{t,\theta}}{\partial \mu_{t-1,\theta}} \\
\end{bmatrix}
=
\vec{I} + 
\begin{bmatrix}
\frac{\partial f(x_{t-1},u_t)}{\partial \mu_{t-1,x}} & \frac{\partial f(x_{t-1},u_t)}{\partial \mu_{t-1,y}} & \frac{\partial f(x_{t-1},u_t)}{\partial \mu_{t-1,\theta}} \\
\frac{\partial f(y_{t-1},u_t)}{\partial \mu_{t-1,x}} & \frac{\partial f(y_{t-1},u_t)}{\partial \mu_{t-1,y}} & \frac{\partial f(y_{t-1},u_t)}{\partial \mu_{t-1,\theta}} \\
\frac{\partial f(\theta_{t-1},u_t)}{\partial \mu_{t-1,x}} & \frac{\partial f(\theta_{t-1},u_t)}{\partial \mu_{t-1,y}} & \frac{\partial f(\theta_{t-1},u_t)}{\partial \mu_{t-1,\theta}} \\
\end{bmatrix}
$$

\paragraph{Aside: Helper Matrix}
Using the same helper matrix as before, we can compute our full-size Jacobian that is the correct size to scale the covariance matrix.  Let $f^{(a)}$ be the first partial derivative of $f$ with respect to $a$.
$$ G_t = \vec{I} + F_x^T
\begin{bmatrix}
f^{(x_{t-1})}(x_{t-1},u_t) & f^{(y_{t-1})}(x_{t-1},u_t) & f^{(\theta_{t-1})}(x_{t-1},u_t) \\
f^{(x_{t-1})}(y_{t-1},u_t) & f^{(y_{t-1})}(y_{t-1},u_t) & f^{(\theta_{t-1})}(y_{t-1},u_t) \\
f^{(x_{t-1})}(\theta_{t-1},u_t) & f^{(y_{t-1})}(\theta_{t-1},u_t) & f^{(\theta_{t-1})}(\theta_{t-1},u_t) \\
\end{bmatrix}
F_x $$

\paragraph{}
\paragraph{}
We must also account for error due to the sensors used to measure our movement.  To do so, we declare a matrix $R$ that defines the error of our sensors as computed empirically.
$$ R = \begin{bmatrix}
\sigma_x^2 & 0 & 0 \\
0 & \sigma_y^2 & 0 \\
0 & 0 & \sigma_\theta^2 \\
\end{bmatrix} $$

In order to update our covariance matrix, we must expand $R$ to be the same size as $\Sigma$, ensuring that all other elements are zero.  The simplest way to do this is to again utilize our helper matrix.  Our Jacobian is used to scale the covariance matrix, and the sensor error is simply added to it.
$$ \overline{\Sigma}_t = G_t\Sigma_{t-1}G_t^T + F_x^TRF_x $$









\subsection{Measurement Update}
At this point, we have a mean $\overline{\mu}_t$ and a covariance $\overline{\Sigma}_t$ that have been updated based on robot movement.  Let us also define a set of landmark measurements $z_t$, where each measurement is provided in polar coordinates relative to the robot position.
$$ z_t^i = \begin{bmatrix}r_t^i\\\phi_t^i\\s_t^i\end{bmatrix} $$

We also declare a set of correspondences $c_t$.  In the simplest case, these are simply $s_t$ from $z_t$.  The true position of a landmark relative to the robot position involves the true location after movement plus noise.
$$ z_t^i = h(\var{Y}_t, z_t^i, c_t^i) + \var{N}(0,Q_t) $$

Once again, we cannot use this equation, so we must approximate it.
$$ h(\var{Y}_t,z_t^i,c_t^i) \approx h(\overline\mu_t, z_t^i, c_t^i) + H_t^i[\var{Y}_t-\overline\mu_t] $$

The process of obtaining the Jacobian $H_t^i$ is similar to the process for obtaining $G_t$, but there are many other steps we must also take.  For each measurement $z_t^i$, we must perform the following steps:

\begin{enumerate}
	\item We must find landmark $j$ that is associated with the correspondence $c_t^i$.  In the case that $c_t^i$ is the same as $s_t^i$, this process is simple.


	\item If this landmark has never been seen before, we must initialize its position.  This is done by converting the polar measurements to cartesian and adding them to the current robot global position.  Note that $s_t^i$ never changes.
	$$
	\begin{bmatrix}\overline\mu_{t,j,x}\\\overline\mu_{t,j,y}\\\overline\mu_{t,j,s}\end{bmatrix}
	=
	\begin{bmatrix}\overline\mu_{t,x}\\\overline\mu_{t,y}\\0\end{bmatrix}
	+
	\begin{bmatrix}r_t^i\cos(\phi_t^i+\overline\mu_{t,\theta})\\r_t^i\sin(\phi_t^i+\overline\mu_{t,\theta}\\s_t^i\end{bmatrix}
	$$


	\item We need the position of the landmark in our map relative to the robot's position.  Note that this step does not involve the measurement $z_t^i$.  This relative position is a measure of where we think the landmark is based on our current map.
	$$ \delta = \begin{bmatrix}\delta_x\\\delta_y\end{bmatrix} = \begin{bmatrix}\overline\mu_{t,j,x}-\overline\mu_{t,x}\\\overline\mu_{t,j,y}-\overline\mu_{t,y}\end{bmatrix} $$


	\item We now use the relative position we just calculated to estimate what we think we \textit{should} have measured based on our map.
	$$ \widehat{z}_t^i = \begin{bmatrix}\sqrt{\delta_x^2+\delta_y^2}\\\arctan(\delta_y,\delta_x)-\overline\mu_{t,\theta}\\\overline\mu_{j,s}\end{bmatrix} $$


	\item We must once again compute the Jacobian of our measurement model.  It will be a matrix of size $3\times(3N+3)$.
	$$
	H_t^i = 
	\begin{bmatrix}
	\frac{\partial r_t^i}{\partial \overline\mu_{t,x}} & \frac{\partial r_t^i}{\partial \overline\mu_{t,y}} & \dots \\
	\frac{\partial \phi_t^i}{\partial \overline\mu_{t,x}} & \dots & \dots \\
	\frac{\partial s_t^i}{\partial \overline\mu_{t,x}} & \dots & \dots \\
	\end{bmatrix}
	$$

	If we examine this entire Jacobian, we will see that most elements are $0$.  Of the elements that are not zero, many of them are similar.  We will now define a value $q = \delta_x^2 + \delta_x^2$ to simplify this Jacobian.  We will also need to define a new helper matrix $F_{xj}$.
	$$
	F_{xj} = 
	\begin{bmatrix}
	1 & 0 & 0 &  ~  0 & \dots & 0 &  ~  0 & 0 & 0 &  ~  0 & \dots & 0 \\
	0 & 1 & 0 &  ~  0 & \dots & 0 &  ~  0 & 0 & 0 &  ~  0 & \dots & 0 \\
	0 & 0 & 1 &  ~  0 & \dots & 0 &  ~  0 & 0 & 0 &  ~  0 & \dots & 0 \\
	0 & 0 & 0 &  ~  0 & \dots & 0 &  ~  1 & 0 & 0 &  ~  0 & \dots & 0 \\
	0 & 0 & 0 &  ~  0 & \dots & 0 &  ~  0 & 1 & 0 &  ~  0 & \dots & 0 \\
	0 & 0 & 0 &  ~  0 & \dots & 0 &  ~  0 & 0 & 1 &  ~  0 & \dots & 0 \\
	\end{bmatrix}
	$$

	This matrix is of size $3\times(3N+3)$, and the spacing of zeros is such that the $j^{th}$ set of $3$ columns after the first three is the special nonzero case.

	The Jacobian can now be simplified using this helper matrix.
	$$
	H_t^i = 
	\begin{bmatrix}
	-\frac{\delta_x}{\sqrt{q}} & -\frac{\delta_y}{\sqrt{q}} &  0 &   \frac{\delta_x}{\sqrt{q}} & \frac{\delta_y}{\sqrt{q}} &  0 \\
	 \frac{\delta_y}{q}        & -\frac{\delta_x}{q}        & -1 &  -\frac{\delta_y}{q}        & \frac{\delta_x}{q}        &  0 \\
	0                          & 0                          &  0 &   0                         & 0                         &  1 \\
	\end{bmatrix}
	F_{xj}
	$$

	We now define an error matrix to represent the noise due to our sensors.
	$$ Q_t = 
	\begin{bmatrix}
	\sigma_r^2 & 0 & 0 \\
	0 & \sigma_\phi^2 & 0 \\
	0 & 0 & \sigma_s^2 \\
	\end{bmatrix}
	$$

	Finally, we are able to compute a matrix representing the error in our sensor measurement, which is of size $3\times3$.
	$$ S_t^i = H_t^i \overline\Sigma_t \left[H_t^i\right]^T + Q_t $$



	\item The Kalman gain is part of all Kalman filters.  It is the result of combining Gaussians.  In our case, it has size $(3N+3)\times(3N+3)$.
	$$ K_t^i = \overline\Sigma_t \left[H_t^i\right]^T\left[S_t^i\right]^{-1} $$



	\item We now update our mean using the landmark measurement $z_t^i$.  The Kalman gain scales the error between our expected landmark location and our measured landmark location.  The difference between measured and expected location is known as the ``innovation''.
	$$ \overline\mu_t = \overline\mu_t + K_t^i (z_t^i - \widehat{z}_t^i) $$



	\item We can now also update the covariance matrix with the information from our landmark measurement.
	$$ \overline\Sigma_t = (I - K_t^iH_t^i) \overline\Sigma_t $$

\end{enumerate}



Once these steps have been completed for every landmark measurement, we can assign the new mean and covariance.  We have now completed one iteration of EKF SLAM.
$$ \mu_t = \overline\mu_t $$
$$ \Sigma_t = \overline\Sigma_t $$






\section{EKF SLAM with Unknown Correspondences}
In the case where it is unknown how many landmarks exist and we cannot uniquely idendtify them, we must adjust our EKF SLAM algorithm.  We need a method to determine whether or not we've seen a landmark before when we measure it.  This method is known as a maximum likelihood (ML) estimation.

Initially, our number of landmarks $N$ is $0$.  As we encounter new landmarks, this variable will increase, and our vectors and matrices will also grow.  The movement update does does not change because it does not involve landmark measurements.

\subsection{Measurement Update}
For each measurement $z_t^i$, we add a new landmark to our map.  Since we have no way to uniquely identify a landmark, we cannot simply check our correspondences.
$$
\begin{bmatrix}\overline\mu_{t,N+1,x}\\\overline\mu_{t,N+1,y}\\0\end{bmatrix}
=
\begin{bmatrix}\overline\mu_{t,x}\\\overline\mu_{t,y}\\0\end{bmatrix}
+
\begin{bmatrix}r_t^i\cos(\phi_t^i+\overline\mu_{t,\theta})\\r_t^i\sin(\phi_t^i+\overline\mu_{t,\theta}\\0\end{bmatrix}
$$


Next, for all landmarks $k \in [1,N+1]$, we compute $\widehat{z}_t^k$, $H_t^k$, and $S_t^k$ the same way as before.  We then compute the Mahalanobis distance between our new measurement $z_t^i$ and current map landmark $\widehat{z}_t^k$.
$$ \Pi_k = \left(z_t^i-\widehat{z}_t^k\right)^T \left[S_t^k\right]^{-1}\left(z_t^i-\widehat{z}_t^k\right) $$

When $k = N+1$, we do not calculate this distance because it would just be $0$.  Instead, we set it to a small threshold that should be determine empirically.
$$ \Pi_{N+1} = \alpha $$

After we've gone through all our map landmarks $k$, we find the landmark $j$ with the minimum Mahalanobis distance $\Pi$.  If this $j^{th}$ landmark was the new map element ($j=N+1$), then we have encountered a new landmark.  Thus, we increment $N$ to account for it.  If any other landmark had the shortest distance ($j\neq N+1$), then our measurement $z_t^i$ corresponded to that landmark, so we do not increment $N$.  We also discard the new values added to our mean vector.

When updating $\overline\mu_t$ and $\overline\Sigma_t$, we use the $H_t^j$ and $S_t^j$ that correspond to the shortest distance.  Note that our covariance matrix will need to increase is size to account for this new landmark.  Our mean was already enlarged to account for this.










\section{FastSLAM}
A problem with EKF SLAM is that it involves lots of matrix multiplications and inversions, making it very computationally expensive.  FastSLAM simplifies EKF SLAM by the use of particle filters.  Specifically, it uses a Rao-Blackwell particle filter.

Whereas EKF SLAM attempts to exactly represent an approximation of reality by using a single mean and covariance, FastSLAM approximately represents an exact reality.  Instead of a massive Gaussian representing everything, each landmark is represented using its own Gaussian.  The EKF treatment of Gaussians is the same, but there are many smallers ones instead of one large one.

% TODO: add diagram to help show this concept



\subsection{Algorithm}
The robot pose and landmark gaussians are represented at time $t$ as a particle $Y_t$.  There are a total of $M$ particles.  Given a total of $N$ landmarks, the $k^{th}$ particle is denoted
$$ Y_t^k = \langle \var{X}_t^k, \mu_{1,t}, \Sigma_{1,t}, \dots, \mu_{N,t}, \Sigma_{N,t} \rangle $$

\noindent where:\\
\indent $\var{X}_t^k$ is the robot pose \\
\indent $\mu_{n,t}$ is the mean of the $n^{th}$ landmark \\
\indent $\Sigma_{n,t}$ is the covariance of the $n^{th}$ landmark \\

Note that the robot pose is not represented as a Guassian.  Instead, each particle has its own idea of what the robot pose is.  When considering all the particles, there is a ``cloud'' of robot poses, and the actual robot pose is somewhere inside it.
% TODO: add plot showing the cloud



The FastSLAM algorithm has a lot of similarities with EKF SLAM, but there are several important differences.  The following steps need to be completed for each particle $k \in M$.

\begin{enumerate}

	\item Update the robot pose using the correct motion model.  Instead of adjusting a covariance matrix, we add Gaussian noise when we compute the new pose.  The standard deviation of this Gaussian noise should depend on the error in the movement sensors.
	% TODO: add equations for pose sampling


	\item The measurement update is different depending on if the landmark has been seen before or not.  If the landmark has never been seen before, we have to initialize its mean and covariance.  Its importance must also be initialized to a default value.
	\begin{enumerate}
		\item Initialize $\mu_{j,t}^k$ using the same method as EKF
		\item Initialize $\Sigma_{j,t}^k$ based on measurement noise, similar to the EKF method
		$$ \Sigma_{j,t}^k = H^{-1}Q_t\left[H^{-1}\right]^T $$
		\item Initialize the importance weight to a default, small value
		$$ w^k = p_0 $$
	\end{enumerate}

	If the landmark has been seen before, we update its mean and covariance before calculating its importance, which is a measure based on the Kalman innovation.
	\begin{enumerate}
		\item Update $\mu_{j,t}^k$ and $\Sigma_{j,t}^k$ using the same method as EKF
		\item Compute the importance factor.
		$$ w^k = det(2\pi S)^{-\frac{1}{2}} e^{-\frac{1}{2} (z_t^i-\widehat{z}_t^j)^T S^{-1} (z_t^i-\widehat{z}_t^j)} $$
	\end{enumerate}

	This process should be completed for each landmark measured, and the importance weights should be added.


	\item After we have handled movement and measurement, we must create a new set of particles.  We resample the particles (with replacement) based on their importance factor.  A particle $Y_k$ has the probability of being chosen determined by
	$$ \frac{w^k}{\sum_{i=1}^{M}w^i} $$

	Once we have a new set of $M$ particles, we have completed one iteration of our FastSLAM algorithm, and we return the new set of particles.



\end{enumerate}









\end{document}

